<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* extension/module/product_list.twig */
class __TwigTemplate_3748cc90b623902be0a2843b650cc72a0b9548ab69cd2a9401e28b2612034e88 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <h1>";
        // line 5
        echo ($context["heading_title"] ?? null);
        echo "</h1>
      <ul class=\"breadcrumb\">
        ";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 8
            echo "        <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 8);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 8);
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 10
        echo "      </ul>
    </div>
  </div>
  <div class=\"container-fluid\">";
        // line 13
        if (($context["error_warning"] ?? null)) {
            // line 14
            echo "    <div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo ($context["error_warning"] ?? null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 18
        echo "    ";
        if (($context["success"] ?? null)) {
            // line 19
            echo "    <div class=\"alert alert-success alert-dismissible\"><i class=\"fa fa-check-circle\"></i> ";
            echo ($context["success"] ?? null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 23
        echo "    <div class=\"row\">
      <div class=\"col-sm-9\">
        <div class=\"panel panel-default\">
          <div class=\"panel-heading\">
            <h3 class=\"panel-title\"><i class=\"fa fa-list\"></i> ";
        // line 27
        echo ($context["text_list"] ?? null);
        echo "</h3>
          </div>
          <div class=\"panel-body\">
            <form action=\"";
        // line 30
        echo ($context["delete"] ?? null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-product\">
              <div class=\"table-responsive\">
                <table class=\"table table-bordered table-hover\">
                  <thead>
                    <tr>
                      <td class=\"text-center\">";
        // line 35
        echo ($context["column_image"] ?? null);
        echo "</td>
                      <td class=\"text-left\">";
        // line 36
        echo ($context["column_name"] ?? null);
        echo "</td>
                      <td class=\"text-right\">";
        // line 37
        echo ($context["column_price"] ?? null);
        echo "</td>
                      <td class=\"text-right\">";
        // line 38
        if (($context["column_orders"] ?? null)) {
            echo " ";
            echo ($context["column_orders"] ?? null);
            echo " ";
        } elseif (($context["column_viewed"] ?? null)) {
            echo " ";
            echo ($context["column_viewed"] ?? null);
            echo " ";
        }
        echo "</td>
                    </tr>
                  </thead>
                  <tbody>
                  
                  ";
        // line 43
        if (($context["products"] ?? null)) {
            // line 44
            echo "                  ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["products"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 45
                echo "                  <tr>
                    <td class=\"text-center\">";
                // line 46
                if (twig_get_attribute($this->env, $this->source, $context["product"], "image", [], "any", false, false, false, 46)) {
                    echo " <img src=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "image", [], "any", false, false, false, 46);
                    echo "\" alt=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 46);
                    echo "\" class=\"img-thumbnail\" /> ";
                } else {
                    echo " <span class=\"img-thumbnail list\"><i class=\"fa fa-camera fa-2x\"></i></span> ";
                }
                echo "</td>
                    <td class=\"text-left\">";
                // line 47
                echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 47);
                echo "</td>
                    <td class=\"text-right\">";
                // line 48
                if (twig_get_attribute($this->env, $this->source, $context["product"], "special", [], "any", false, false, false, 48)) {
                    echo " <span style=\"text-decoration: line-through;\">";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 48);
                    echo "</span><br/>
                      <div class=\"text-danger\">";
                    // line 49
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "special", [], "any", false, false, false, 49);
                    echo "</div>
                      ";
                } else {
                    // line 51
                    echo "                      ";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 51);
                    echo "
                      ";
                }
                // line 52
                echo "</td>
                    <td class=\"text-right\">";
                // line 53
                if ((twig_get_attribute($this->env, $this->source, $context["product"], "orders", [], "any", false, false, false, 53) <= 0)) {
                    echo " <span class=\"label label-warning\">";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "orders", [], "any", false, false, false, 53);
                    echo "</span> ";
                } elseif ((twig_get_attribute($this->env, $this->source, $context["product"], "orders", [], "any", false, false, false, 53) <= 5)) {
                    echo " <span class=\"label label-danger\">";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "orders", [], "any", false, false, false, 53);
                    echo "</span> ";
                } else {
                    echo " <span class=\"label label-success\">";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "orders", [], "any", false, false, false, 53);
                    echo "</span> ";
                }
                echo " ";
                if ((twig_get_attribute($this->env, $this->source, $context["product"], "viewed", [], "any", false, false, false, 53) <= 0)) {
                    echo " <span class=\"label label-warning\">";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "viewed", [], "any", false, false, false, 53);
                    echo "</span> ";
                } elseif ((twig_get_attribute($this->env, $this->source, $context["product"], "viewed", [], "any", false, false, false, 53) <= 5)) {
                    echo " <span class=\"label label-danger\">";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "viewed", [], "any", false, false, false, 53);
                    echo "</span> ";
                } else {
                    echo " <span class=\"label label-success\">";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "viewed", [], "any", false, false, false, 53);
                    echo "</span> ";
                }
                echo " </td>
                    
                  </tr>
                  ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 57
            echo "                  ";
        } else {
            // line 58
            echo "                  <tr>
                    <td class=\"text-center\" colspan=\"8\">";
            // line 59
            echo ($context["text_no_results"] ?? null);
            echo "</td>
                  </tr>
                  ";
        }
        // line 62
        echo "                    </tbody>
                  
                </table>
              </div>
            </form>
            <div class=\"row\">
              <div class=\"col-sm-6 text-left\">";
        // line 68
        echo ($context["pagination"] ?? null);
        echo "</div>
              <div class=\"col-sm-6 text-right\">";
        // line 69
        echo ($context["results"] ?? null);
        echo "</div>
            </div>
          </div>
        </div>
      </div>
      <div class=\"col-sm-3\">
          <div class=\"panel panel-default\">
            <div class=\"panel-heading\">
              <h3 class=\"panel-title\" style=\"color:DodgerBlue;\"> <i class=\"fa fa-filter\"></i>";
        // line 77
        echo ($context["text_filtur"] ?? null);
        echo "</h3>
            </div>
            <div class=\"panel-body\">
              <div class=\"form-group\">
                <label for=\"input-category\" class=\"control-label\"> Category </label>
                <input type=\"text\" name=\"name\" value=\"";
        // line 82
        echo ($context["name"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_category"] ?? null);
        echo "\" id=\"input-category\" class=\"form-control\"/> <input type=\"hidden\" name=\"category_id\" value=\"";
        echo ($context["category_id"] ?? null);
        echo "\"/>
                <label class=\"control-label\" for=\"input-limit\"> Show </label>
                <input type=\"text\" name=\"limit\" value=\"";
        // line 84
        echo ($context["limit"] ?? null);
        echo "\" placeholder=\"Enter limit of product to show\"  id=\"input-limit\" class=\"form-control\" />
              </div>
              <div class=\"form-group text-right\">
                <button type=\"button\" id=\"button-filter\" class=\"btn btn-default\"><i class=\"fa fa-filter\"></i> ";
        // line 87
        echo ($context["button_filter"] ?? null);
        echo " </button>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
";
        // line 95
        if ((($context["function"] ?? null) == "topViewProduct")) {
            // line 96
            echo "<script type=\"text/javascript\"><!--
\$('#button-filter').on('click', function() {
\tvar url = '';

  var limit = \$('input[name=\\'limit\\']').val();
  
  if (limit) {
    url += '&limit=' + encodeURIComponent(limit);
  }
  
  var category_id = \$('input[name=\\'category_id\\']').val();
  
  if (category_id) {
    url += '&category_id=' + encodeURIComponent(category_id);
  }

  location = 'index.php?route=extension/module/reports/topViewProduct&user_token=";
            // line 112
            echo ($context["user_token"] ?? null);
            echo "' + url;
  
  });
  //--></script>
  ";
        }
        // line 117
        echo "
  ";
        // line 118
        if ((($context["function"] ?? null) == "lowestViewProduct")) {
            // line 119
            echo "<script type=\"text/javascript\"><!--
\$('#button-filter').on('click', function() {
\tvar url = '';

  var limit = \$('input[name=\\'limit\\']').val();
  
  if (limit) {
    url += '&limit=' + encodeURIComponent(limit);
  }
  
  var category_id = \$('input[name=\\'category_id\\']').val();
  
  if (category_id) {
    url += '&category_id=' + encodeURIComponent(category_id);
  }

  location = 'index.php?route=extension/module/reports/lowestViewProduct&user_token=";
            // line 135
            echo ($context["user_token"] ?? null);
            echo "' + url;
  
  });
  //--></script>
  ";
        }
        // line 140
        echo "
  ";
        // line 141
        if ((($context["function"] ?? null) == "highestSellingProduct")) {
            // line 142
            echo "<script type=\"text/javascript\"><!--
\$('#button-filter').on('click', function() {
\tvar url = '';

  var limit = \$('input[name=\\'limit\\']').val();
  
  if (limit) {
    url += '&limit=' + encodeURIComponent(limit);
  }
  
 var category_id = \$('input[name=\\'category_id\\']').val();
  
  if (category_id) {
    url += '&category_id=' + encodeURIComponent(category_id);
  }

  location = 'index.php?route=extension/module/reports/highestSellingProduct&user_token=";
            // line 158
            echo ($context["user_token"] ?? null);
            echo "' + url;
  
  });
  //--></script>
  ";
        }
        // line 163
        echo "
  ";
        // line 164
        if ((($context["function"] ?? null) == "lowestSellingProduct")) {
            // line 165
            echo "<script type=\"text/javascript\"><!--
\$('#button-filter').on('click', function() {
\tvar url = '';

  var limit = \$('input[name=\\'limit\\']').val();
  
  if (limit) {
    url += '&limit=' + encodeURIComponent(limit);
  }
  
  var category_id = \$('input[name=\\'category_id\\']').val();
  
  if (category_id) {
    url += '&category_id=' + encodeURIComponent(category_id);
  }

  location = 'index.php?route=extension/module/reports/lowestSellingProduct&user_token=";
            // line 181
            echo ($context["user_token"] ?? null);
            echo "' + url;
  
  });
  //--></script>
  ";
        }
        // line 186
        echo "
    // Category
  <script type=\"text/javascript\"><!--
  \$('input[name=\\'name\\']').autocomplete({
\t  'source': function(request, response) {
\t\t  \$.ajax({
\t\t\t  url: 'index.php?route=extension/module/reports/autocomplete&user_token=";
        // line 192
        echo ($context["user_token"] ?? null);
        echo "&filter_category=' + encodeURIComponent(request),
\t\t\t  dataType: 'json',
\t\t\t  success: function(json) {
\t\t\t\t  json.unshift({
\t\t\t\t\t  category_id: 0,
\t\t\t\t\t  name: '";
        // line 197
        echo ($context["text_none"] ?? null);
        echo "'
\t\t\t\t  });

\t\t\t\t  response(\$.map(json, function(item) {
\t\t\t\t\t  return {
\t\t\t\t\t\t  label: item['name'],
\t\t\t\t\t\t  value: item['category_id']
\t\t\t\t\t  }
\t\t\t\t  }));
\t\t\t  }
\t\t  });
\t  },
\t  'select': function(item) {
\t\t  \$('input[name=\\'name\\']').val(item['label']);
\t\t  \$('input[name=\\'category_id\\']').val(item['value']);
\t  }
  });
  //--></script>
";
        // line 215
        echo ($context["footer"] ?? null);
        echo " ";
    }

    public function getTemplateName()
    {
        return "extension/module/product_list.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  448 => 215,  427 => 197,  419 => 192,  411 => 186,  403 => 181,  385 => 165,  383 => 164,  380 => 163,  372 => 158,  354 => 142,  352 => 141,  349 => 140,  341 => 135,  323 => 119,  321 => 118,  318 => 117,  310 => 112,  292 => 96,  290 => 95,  279 => 87,  273 => 84,  264 => 82,  256 => 77,  245 => 69,  241 => 68,  233 => 62,  227 => 59,  224 => 58,  221 => 57,  185 => 53,  182 => 52,  176 => 51,  171 => 49,  165 => 48,  161 => 47,  149 => 46,  146 => 45,  141 => 44,  139 => 43,  123 => 38,  119 => 37,  115 => 36,  111 => 35,  103 => 30,  97 => 27,  91 => 23,  83 => 19,  80 => 18,  72 => 14,  70 => 13,  65 => 10,  54 => 8,  50 => 7,  45 => 5,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "extension/module/product_list.twig", "");
    }
}
