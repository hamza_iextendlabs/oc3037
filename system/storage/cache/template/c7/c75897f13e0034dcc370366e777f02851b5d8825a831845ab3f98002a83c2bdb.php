<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* extension/module/seller_list.twig */
class __TwigTemplate_33cf4df4c7b1639e403460d5fc52358907a27a5e2ce00f44da65d775889f86fa extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\"><a href=\"";
        // line 5
        echo ($context["add"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_add"] ?? null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-plus\"></i></a>
        <button type=\"button\" data-toggle=\"tooltip\" title=\"";
        // line 6
        echo ($context["button_delete"] ?? null);
        echo "\" class=\"btn btn-danger\" onclick=\"confirm('";
        echo ($context["text_confirm"] ?? null);
        echo "') ? \$('#form-seller').submit() : false;\"><i class=\"fa fa-trash-o\"></i></button>
      </div>
      <h1>";
        // line 8
        echo ($context["text_add"] ?? null);
        echo "</h1>
    </div>
  </div>
  <div class=\"container-fluid\">
    ";
        // line 12
        if (($context["error_warning"] ?? null)) {
            // line 13
            echo "    <div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo ($context["error_warning"] ?? null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 17
        echo "    ";
        if (($context["success"] ?? null)) {
            // line 18
            echo "    <div class=\"alert alert-success alert-dismissible\"><i class=\"fa fa-check-circle\"></i> ";
            echo ($context["success"] ?? null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 22
        echo "    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\"><i class=\"fa fa-list\"></i> ";
        // line 24
        echo ($context["text_list"] ?? null);
        echo "</h3>
      </div>
      <div class=\"panel-body\">
        <form action=\"";
        // line 27
        echo ($context["delete"] ?? null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-seller\">
          <div class=\"table-responsive\">
            <table class=\"table table-bordered table-hover\">
              <thead>
                <tr>
                  <td style=\"width: 1px;\" class=\"text-center\"><input type=\"checkbox\" onclick=\"\$('input[name*=\\'selected\\']').prop('checked', this.checked);\" /></td>
                  <td class=\"text-center\" style=\"width: 80px;\">Image</td>
                  <td class=\"text-left\">";
        // line 34
        if ((($context["sort"] ?? null) == "id.name")) {
            // line 35
            echo "                    <a href=\"";
            echo ($context["sort_title"] ?? null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, ($context["order"] ?? null));
            echo "\">";
            echo ($context["column_name"] ?? null);
            echo "</a>
                    ";
        } else {
            // line 37
            echo "                    <a href=\"";
            echo ($context["sort_title"] ?? null);
            echo "\">";
            echo ($context["column_seller_name"] ?? null);
            echo "</a>
                    ";
        }
        // line 38
        echo "</td>
                  <td class=\"text-right\">";
        // line 39
        echo ($context["column_action"] ?? null);
        echo "</td>
                </tr>
              </thead>
              <tbody>
                ";
        // line 43
        if (($context["sellers"] ?? null)) {
            // line 44
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["sellers"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["seller"]) {
                // line 45
                echo "                <tr>
                  <td class=\"text-center\">";
                // line 46
                if (twig_in_filter(twig_get_attribute($this->env, $this->source, $context["seller"], "seller_id", [], "any", false, false, false, 46), ($context["selected"] ?? null))) {
                    // line 47
                    echo "                    <input type=\"checkbox\" name=\"selected[]\" value=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["seller"], "seller_id", [], "any", false, false, false, 47);
                    echo "\" checked=\"checked\" />
                    ";
                } else {
                    // line 49
                    echo "                    <input type=\"checkbox\" name=\"selected[]\" value=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["seller"], "seller_id", [], "any", false, false, false, 49);
                    echo "\" />
                    ";
                }
                // line 50
                echo "</td>
                  <td class=\"text-center\">";
                // line 51
                if (twig_get_attribute($this->env, $this->source, $context["seller"], "image", [], "any", false, false, false, 51)) {
                    echo " <img src=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["seller"], "image", [], "any", false, false, false, 51);
                    echo "\" alt=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["seller"], "name", [], "any", false, false, false, 51);
                    echo "\" class=\"img-thumbnail\" /> ";
                } else {
                    echo " <span class=\"img-thumbnail list\"><i class=\"fa fa-camera fa-2x\"></i></span> ";
                }
                echo "</td>
                  <td class=\"text-left\">";
                // line 52
                echo twig_get_attribute($this->env, $this->source, $context["seller"], "seller_name", [], "any", false, false, false, 52);
                echo "</td>
                  <td class=\"text-right\"><a href=\"";
                // line 53
                echo twig_get_attribute($this->env, $this->source, $context["seller"], "edit", [], "any", false, false, false, 53);
                echo "\" data-toggle=\"tooltip\" title=\"";
                echo ($context["button_edit"] ?? null);
                echo "\" class=\"btn btn-primary\"><i class=\"fa fa-pencil\"></i></a></td>
                </tr>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['seller'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 56
            echo "                ";
        } else {
            // line 57
            echo "                <tr>
                  <td class=\"text-center\" colspan=\"4\">";
            // line 58
            echo ($context["text_no_results"] ?? null);
            echo "</td>
                </tr>
                ";
        }
        // line 61
        echo "              </tbody>
            </table>
          </div>
        </form>
        <div class=\"row\">
          <div class=\"col-sm-6 text-left\">";
        // line 66
        echo ($context["pagination"] ?? null);
        echo "</div>
          <div class=\"col-sm-6 text-right\">";
        // line 67
        echo ($context["results"] ?? null);
        echo "</div>
        </div>
      </div>
    </div>
  </div>
</div>
";
        // line 73
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "extension/module/seller_list.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  222 => 73,  213 => 67,  209 => 66,  202 => 61,  196 => 58,  193 => 57,  190 => 56,  179 => 53,  175 => 52,  163 => 51,  160 => 50,  154 => 49,  148 => 47,  146 => 46,  143 => 45,  138 => 44,  136 => 43,  129 => 39,  126 => 38,  118 => 37,  108 => 35,  106 => 34,  96 => 27,  90 => 24,  86 => 22,  78 => 18,  75 => 17,  67 => 13,  65 => 12,  58 => 8,  51 => 6,  45 => 5,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "extension/module/seller_list.twig", "");
    }
}
