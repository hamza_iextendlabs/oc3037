<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* extension/module/reports.twig */
class __TwigTemplate_18266c9b2f56c3aff6f40b911a2629677b4f5a61fe905315a429e43a30de7f20 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
    <div class=\"page-header\">
        <div class=\"container-fluid\">
        <div class=\"pull-right\">
            <a href=\"";
        // line 6
        echo ($context["report_setting"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["text_setting"] ?? null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-cog fw\"></i></a>
        </div>
            <h1>";
        // line 8
        echo ($context["heading_title"] ?? null);
        echo "</h1>
            <ul class=\"breadcrumb\">
                ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 11
            echo "                <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 11);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 11);
            echo "</a></li>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "            </ul>
        </div>
        ";
        // line 15
        if (($context["isFirstLoad"] ?? null)) {
            // line 16
            echo "            <div class=\"col-md-12\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid#4cb64c; border-radius: 15px; margin-left: 10px;\">
                    <div class=\"text-success text-center mt-3 panel-body\">
                        <h4><b>";
            // line 19
            echo ($context["isFirstLoad"] ?? null);
            echo "</b></h4>
                    </div>
                </div>
            </div>
            ";
        }
        // line 24
        echo "    </div>
    <div class=\"jumbotron\">
        <div class=\"panel panel-default\">
            <div class=\"panel-heading\">
            <div class=\"pull-right\"><a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"><h2><i class=\"fa fa-calendar\"></i> <i class=\"caret\"></h2></i></a>
                <ul id=\"range\" class=\"dropdown-menu dropdown-menu-right\">
                    ";
        // line 30
        if ((($context["text_sales_report"] ?? null) == "Daily Sales Report")) {
            // line 31
            echo "                  <li id=\"day\" class=\"active\"><a>Today</a></li> <input type=\"hidden\" value=\"";
            echo ($context["day_start"] ?? null);
            echo "\" name=\"day_start\"> <input type=\"hidden\" value=\"";
            echo ($context["day_end"] ?? null);
            echo "\" name=\"day_end\">
                    ";
        } else {
            // line 33
            echo "                  <li id=\"day\"><a>Today</a></li> <input type=\"hidden\" value=\"";
            echo ($context["day_start"] ?? null);
            echo "\" name=\"day_start\"> <input type=\"hidden\" value=\"";
            echo ($context["day_end"] ?? null);
            echo "\" name=\"day_end\">
                    ";
        }
        // line 35
        echo "                    ";
        if ((($context["text_sales_report"] ?? null) == "Monthly Sales Report")) {
            // line 36
            echo "                  <li id=\"month\" class=\"active\"><a>Month</a></li> <input type=\"hidden\" value=\"";
            echo ($context["month_start"] ?? null);
            echo "\" name=\"month_start\"> <input type=\"hidden\" value=\"";
            echo ($context["month_end"] ?? null);
            echo "\" name=\"month_end\">
                    ";
        } else {
            // line 38
            echo "                  <li id=\"month\"><a>Month</a></li> <input type=\"hidden\" value=\"";
            echo ($context["month_start"] ?? null);
            echo "\" name=\"month_start\"> <input type=\"hidden\" value=\"";
            echo ($context["month_end"] ?? null);
            echo "\" name=\"month_end\">
                    ";
        }
        // line 40
        echo "                    ";
        if ((($context["text_sales_report"] ?? null) == "Yearly Sales Report")) {
            // line 41
            echo "                  <li id=\"year\" class=\"active\"><a>Year</a></li> <input type=\"hidden\" value=\"";
            echo ($context["year_start"] ?? null);
            echo "\" name=\"year_start\"> <input type=\"hidden\" value=\"";
            echo ($context["year_end"] ?? null);
            echo "\" name=\"year_end\">
                    ";
        } else {
            // line 43
            echo "                  <li id=\"year\"><a>Year</a></li> <input type=\"hidden\" value=\"";
            echo ($context["year_start"] ?? null);
            echo "\" name=\"year_start\"> <input type=\"hidden\" value=\"";
            echo ($context["year_end"] ?? null);
            echo "\" name=\"year_end\">
                    ";
        }
        // line 45
        echo "                
                </ul>
              </div>
            <h2 class=\"panel-tital\">";
        // line 48
        echo ($context["text_sales_report"] ?? null);
        echo "</h2>
            </div>
        </div>
              <div class=\"panel-heading\">
                <h3 class=\"panel-title\" style=\"color:DodgerBlue;\"> <i class=\"fa fa-filter\"></i>";
        // line 52
        echo ($context["custom_filter"] ?? null);
        echo "</h3>
              </div>
              <div class=\"panel-body\">
                <div class=\"container-fluid\">
                  <div class=\"form-group\">
                    <label for=\"input-date-start\" class=\"col-sm-6 control-label\">";
        // line 57
        echo ($context["custom_date_start"] ?? null);
        echo "</label>
                    <div class=\"col-sm-12\">
                      <div class=\"input-group datetime\">
                        <input type=\"text\" name=\"start\" value=\"";
        // line 60
        echo ($context["start"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["custom_date_start"] ?? null);
        echo "\" data-date-format=\"YYYY-MM-DD HH:mm:ss\" id=\"input-date-start\" class=\"form-control\" class=\"col-sm-2\"/> <span class=\"input-group-btn\">
                        <button class=\"btn btn-default\" type=\"button\"><i class=\"fa fa-calendar\"></i></button>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class=\"container-fluid\">
                  <div class=\"form-group\">
                    <label for=\"input-date-end\" class=\"col-sm-6 control-label\">";
        // line 69
        echo ($context["custom_date_end"] ?? null);
        echo "</label>
                    <div class=\"col-sm-12\">
                      <div class=\"input-group datetime\">
                        <input type=\"text\" name=\"end\" value=\"";
        // line 72
        echo ($context["end"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["custom_date_end"] ?? null);
        echo "\" data-date-format=\"YYYY-MM-DD HH:mm:ss\" id=\"input-date-end\" class=\"form-control\" class=\"col-sm-2\"/> <span class=\"input-group-btn\">
                        <button class=\"btn btn-default\" type=\"button\"><i class=\"fa fa-calendar\"></i></button>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class=\"form-group text-right\">
                  <button type=\"button\" id=\"button-custom\" class=\"btn btn-default\"><i class=\"fa fa-filter\"></i> ";
        // line 80
        echo ($context["button_filter"] ?? null);
        echo " </button>
                </div>
        </div>
        <div class=\"row w-100 container-fluid\">
            ";
        // line 84
        if ((($context["order_status"] ?? null) == "on")) {
            // line 85
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid#f3a638; border-radius: 15px; height: 185px;\">
                    <div class=\"text-warning text-center mt-3 panel-body\">
                        <h4>";
            // line 88
            echo ($context["text_total_order"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-warning text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 91
            echo ($context["total_order"] ?? null);
            echo "</h1>
                        <a href=\"";
            // line 92
            echo ($context["href_order"] ?? null);
            echo "\"> <h5 class=\"text-warning\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 97
        echo "            ";
        if ((($context["sales_status"] ?? null) == "on")) {
            // line 98
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid #4cb657; border-radius: 15px; height: 185px;\">
                    <div class=\"text-success text-center mt-3 panel-body\">
                        <h4>";
            // line 101
            echo ($context["text_sales"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-success text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 104
            echo ($context["sales"] ?? null);
            echo "</h1>
                    </div>
                </div>
            </div>
            ";
        }
        // line 109
        echo "            ";
        if ((($context["return_status"] ?? null) == "on")) {
            // line 110
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid #e3503e; border-radius: 15px; height: 185px;\">
                    <div class=\"text-danger text-center mt-3 panel-body\">
                        <h4>";
            // line 113
            echo ($context["text_return"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-danger text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 116
            echo ($context["return"] ?? null);
            echo "</h1>
                        <a href=\"";
            // line 117
            echo ($context["href_refunded"] ?? null);
            echo "\"> <h5 class=\"text-danger\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 122
        echo "            ";
        if ((($context["tax_status"] ?? null) == "on")) {
            // line 123
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid #31708f; border-radius: 15px; height: 185px;\">
                    <div class=\"text-info text-center mt-3 panel-body\">
                        <h4>";
            // line 126
            echo ($context["text_tax"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-info text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 129
            echo ($context["tax"] ?? null);
            echo "</h1>
                    </div>
                </div>
            </div>
            ";
        }
        // line 134
        echo "            ";
        if ((($context["complete_order_status"] ?? null) == "on")) {
            // line 135
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid#f3a638; border-radius: 15px; height: 185px;\">
                    <div class=\"text-warning text-center mt-3 panel-body\">
                        <h4>";
            // line 138
            echo ($context["text_complete_order"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-warning text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 141
            echo ($context["total_complete_order"] ?? null);
            echo "</h1>
                        <a href=\"";
            // line 142
            echo ($context["href_complete"] ?? null);
            echo "\"> <h5 class=\"text-warning\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 147
        echo "            ";
        if ((($context["pending_order_status"] ?? null) == "on")) {
            // line 148
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid #4cb657; border-radius: 15px; height: 185px;\">
                    <div class=\"text-success text-center mt-3 panel-body\">
                        <h4>";
            // line 151
            echo ($context["text_pending_order"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-success text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 154
            echo ($context["total_panding_order"] ?? null);
            echo "</h1>
                        <a href=\"";
            // line 155
            echo ($context["href_panding"] ?? null);
            echo "\"> <h5 class=\"text-success\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 160
        echo "            ";
        if ((($context["canceled_order_status"] ?? null) == "on")) {
            // line 161
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid #e3503e; border-radius: 15px; height: 185px;\">
                    <div class=\"text-danger text-center mt-3 panel-body\">
                        <h4>";
            // line 164
            echo ($context["text_canceled_order"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-danger text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 167
            echo ($context["total_canceled_order"] ?? null);
            echo "</h1>
                        <a href=\"";
            // line 168
            echo ($context["href_canceled"] ?? null);
            echo "\"> <h5 class=\"text-danger\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 173
        echo "            ";
        if ((($context["refunded_order_status"] ?? null) == "on")) {
            // line 174
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid #31708f; border-radius: 15px; height: 185px;\">
                    <div class=\"text-info text-center mt-3 panel-body\">
                        <h4>";
            // line 177
            echo ($context["text_refunded_order"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-info text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 180
            echo ($context["total_refunded_order"] ?? null);
            echo "</h1>
                        <a href=\"";
            // line 181
            echo ($context["href_refunded"] ?? null);
            echo "\"> <h5 class=\"text-info\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 186
        echo "        </div>
        <div class=\"panel panel-default\">
            <div class=\"panel-heading\">
                <h2 class=\"panel-tital\">";
        // line 189
        echo ($context["text_product_report"] ?? null);
        echo "</h2>
            </div>
        </div>
        <div class=\"row w-100 container-fluid\">
            ";
        // line 193
        if ((($context["product_status"] ?? null) == "on")) {
            // line 194
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid#f3a638; border-radius: 15px; height: 185px;\">
                    <div class=\"text-warning text-center mt-3 panel-body\">
                        <h4>";
            // line 197
            echo ($context["text_total_product"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-warning text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 200
            echo ($context["total_product"] ?? null);
            echo "</h1>
                        <a href=\"";
            // line 201
            echo ($context["href_product"] ?? null);
            echo "\"> <h5 class=\"text-warning\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 206
        echo "            ";
        if ((($context["outofstock_product_status"] ?? null) == "on")) {
            // line 207
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid #4cb657; border-radius: 15px; height: 185px;\">
                    <div class=\"text-success text-center mt-3 panel-body\">
                        <h4>";
            // line 210
            echo ($context["text_total_outOfStock_product"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-success text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 213
            echo ($context["total_OutOfStock_product"] ?? null);
            echo "</h1>
                        <a href=\"";
            // line 214
            echo ($context["href_outofstock_product"] ?? null);
            echo "\"> <h5 class=\"text-success\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 219
        echo "            ";
        if ((($context["lowestselling_product_status"] ?? null) == "on")) {
            // line 220
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\"
                    style=\"border: 1px solid #e3503e; border-radius: 15px; height: 185px;\">
                    <div class=\"text-danger text-center mt-3 panel-body\">
                        <h4>";
            // line 224
            echo ($context["text_lowest_selling_product"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-danger text-center mt-2 panel-body\">
                        <h2><b>";
            // line 227
            echo ($context["total_LowestSelling_product"] ?? null);
            echo "</b></h2>
                        <a href=\"";
            // line 228
            echo ($context["href_lowestselling_product"] ?? null);
            echo "\"> <h5 class=\"text-danger\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 233
        echo "            ";
        if ((($context["highestselling_product_status"] ?? null) == "on")) {
            // line 234
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\"
                    style=\"border: 1px solid #31708f; border-radius: 15px; height: 185px;\">
                    <div class=\"text-info text-center mt-3 panel-body\">
                        <h4>";
            // line 238
            echo ($context["text_highest_selling_product"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-info text-center mt-2 panel-body\">
                        <h2><b>";
            // line 241
            echo ($context["total_HighestSelling_product"] ?? null);
            echo "</b></h2>
                        <a href=\"";
            // line 242
            echo ($context["href_highestselling_product"] ?? null);
            echo "\"> <h5 class=\"text-info\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 247
        echo "            ";
        if ((($context["topview_product_status"] ?? null) == "on")) {
            // line 248
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\"
                    style=\"border: 1px solid#f3a638; border-radius: 15px; height: 185px;\">
                    <div class=\"text-warning text-center mt-3 panel-body\">
                        <h4>";
            // line 252
            echo ($context["text_TopView_product"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-warning text-center mt-2 panel-body\">
                        <h2><b>";
            // line 255
            echo ($context["total_TopView_product"] ?? null);
            echo "</b></h2>
                        <a href=\"";
            // line 256
            echo ($context["href_topView_product"] ?? null);
            echo "\"> <h5 class=\"text-warning\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 261
        echo "            ";
        if ((($context["lowestview_product_status"] ?? null) == "on")) {
            // line 262
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\"
                    style=\"border: 1px solid #4cb657; border-radius: 15px; height: 185px;\">
                    <div class=\"text-success text-center mt-3 panel-body\">
                        <h4>";
            // line 266
            echo ($context["text_LowestView_product"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-success text-center mt-2 panel-body\">
                        <h2><b>";
            // line 269
            echo ($context["total_LowestView_product"] ?? null);
            echo "</b></h2>
                        <a href=\"";
            // line 270
            echo ($context["href_lowestview_product"] ?? null);
            echo "\"> <h5 class=\"text-success\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 275
        echo "        </div>
        <div class=\"panel panel-default\">
            <div class=\"panel-heading\">
                <h2 class=\"panel-tital\">";
        // line 278
        echo ($context["text_category_report"] ?? null);
        echo "</h2>
            </div>
        </div>
        <div class=\"row w-100 container-fluid\">
            ";
        // line 282
        if ((($context["customers_status"] ?? null) == "on")) {
            // line 283
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid#f3a638; border-radius: 15px; height: 185px;\">
                    <div class=\"text-warning text-center mt-3 panel-body\">
                        <h4>";
            // line 286
            echo ($context["text_total_customer"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-warning text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 289
            echo ($context["total_customer"] ?? null);
            echo "</h1>
                        <a href=\"";
            // line 290
            echo ($context["href_customer"] ?? null);
            echo "\"> <h5 class=\"text-warning\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 295
        echo "            ";
        if ((($context["category_status"] ?? null) == "on")) {
            // line 296
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid #4cb657; border-radius: 15px; height: 185px;\">
                    <div class=\"text-success text-center mt-3 panel-body\">
                        <h4>";
            // line 299
            echo ($context["text_total_category"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-success text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 302
            echo ($context["total_category"] ?? null);
            echo "</h1>
                        <a href=\"";
            // line 303
            echo ($context["href_category"] ?? null);
            echo "\"> <h5 class=\"text-success\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 308
        echo "            ";
        if ((($context["enable_category_status"] ?? null) == "on")) {
            // line 309
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid #e3503e; border-radius: 15px; height: 185px;\">
                    <div class=\"text-danger text-center mt-3 panel-body\">
                        <h4>";
            // line 312
            echo ($context["text_total_enable_category"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-danger text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 315
            echo ($context["total_enable_category"] ?? null);
            echo "</h1>
                    </div>
                </div>
            </div>
            ";
        }
        // line 320
        echo "            ";
        if ((($context["disable_category_status"] ?? null) == "on")) {
            // line 321
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid #31708f; border-radius: 15px; height: 185px;\">
                    <div class=\"text-info text-center mt-3 panel-body\">
                        <h4>";
            // line 324
            echo ($context["text_total_disable_category"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-info text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 327
            echo ($context["total_disable_category"] ?? null);
            echo "</h1>
                    </div>
                </div>
            </div>
            ";
        }
        // line 332
        echo "            ";
        if ((($context["nanufacturer_status"] ?? null) == "on")) {
            // line 333
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid#f3a638; border-radius: 15px; height: 185px;\">
                    <div class=\"text-warning text-center mt-3 panel-body\">
                        <h4>";
            // line 336
            echo ($context["text_total_manufacturer"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-warning text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 339
            echo ($context["total_manufacturer"] ?? null);
            echo "</h1>
                    </div>
                </div>
            </div>
            ";
        }
        // line 344
        echo "            ";
        if ((($context["topbrand_status"] ?? null) == "on")) {
            // line 345
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\"
                    style=\"border: 1px solid #4cb657; border-radius: 15px; height: 185px;\">
                    <div class=\"text-success text-center mt-3 panel-body\">
                        <h4>";
            // line 349
            echo ($context["text_top_brand"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-success text-center mt-2 panel-body\">
                        <h2><b>";
            // line 352
            echo ($context["top_brand"] ?? null);
            echo "</b></h2>
                    </div>
                </div>
            </div>
            ";
        }
        // line 357
        echo "        </div>
    </div>
</div>
</div>
<script type=\"text/javascript\"><!--
    \$('#day').on('click', function() {
        
        var url = '';
    
      var day_start = \$('input[name=\\'day_start\\']').val();
    
        if (day_start) {
            url += '&day_start=' + (day_start);
        }
      
      var day_end = \$('input[name=\\'day_end\\']').val();
    
        if (day_end) {
            url += '&day_end=' + (day_end);
        }
    
          location = 'index.php?route=extension/module/reports/info&user_token=";
        // line 378
        echo ($context["user_token"] ?? null);
        echo "' + url;
    });
    //--></script>

<script type=\"text/javascript\"><!--
    \$('#month').on('click', function() {
        var url = '';
    
      var month_start = \$('input[name=\\'month_start\\']').val();
    
        if (month_start) {
            url += '&month_start=' + encodeURIComponent(month_start);
        }
      
      var month_end = \$('input[name=\\'month_end\\']').val();
    
        if (month_end) {
            url += '&month_end=' + encodeURIComponent(month_end);
        }
    
          location = 'index.php?route=extension/module/reports/info&user_token=";
        // line 398
        echo ($context["user_token"] ?? null);
        echo "' + url;
    });
    //--></script>

<script type=\"text/javascript\"><!--
    \$('#year').on('click', function() {
        var url = '';
    
      var year_start = \$('input[name=\\'year_start\\']').val();
    
        if (year_start) {
            url += '&year_start=' + encodeURIComponent(year_start);
        }
      
      var year_end = \$('input[name=\\'year_end\\']').val();
    
        if (year_end) {
            url += '&year_end=' + encodeURIComponent(year_end);
        }
    
          location = 'index.php?route=extension/module/reports/info&user_token=";
        // line 418
        echo ($context["user_token"] ?? null);
        echo "' + url;
    });
    //--></script>
      <script type=\"text/javascript\"><!--
        \$('.datetime').datetimepicker({
            language: '";
        // line 423
        echo ($context["datepicker"] ?? null);
        echo "',
            pickDate: true
        });
    //--></script> 
    <script type=\"text/javascript\"><!--
        \$('#button-custom').on('click', function() {
            var url = '';
        
          var custom_date_start = \$('input[name=\\'start\\']').val();
        
            if (custom_date_start) {
                url += '&custom_date_start=' + encodeURIComponent(custom_date_start);
            }
          
          var custom_date_end = \$('input[name=\\'end\\']').val();
        
            if (custom_date_end) {
                url += '&custom_date_end=' + encodeURIComponent(custom_date_end);
            }
          
              location = 'index.php?route=extension/module/reports/info&user_token=";
        // line 443
        echo ($context["user_token"] ?? null);
        echo "' + url;
        });
        //--></script>
";
        // line 446
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "extension/module/reports.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  878 => 446,  872 => 443,  849 => 423,  841 => 418,  818 => 398,  795 => 378,  772 => 357,  764 => 352,  758 => 349,  752 => 345,  749 => 344,  741 => 339,  735 => 336,  730 => 333,  727 => 332,  719 => 327,  713 => 324,  708 => 321,  705 => 320,  697 => 315,  691 => 312,  686 => 309,  683 => 308,  673 => 303,  669 => 302,  663 => 299,  658 => 296,  655 => 295,  645 => 290,  641 => 289,  635 => 286,  630 => 283,  628 => 282,  621 => 278,  616 => 275,  606 => 270,  602 => 269,  596 => 266,  590 => 262,  587 => 261,  577 => 256,  573 => 255,  567 => 252,  561 => 248,  558 => 247,  548 => 242,  544 => 241,  538 => 238,  532 => 234,  529 => 233,  519 => 228,  515 => 227,  509 => 224,  503 => 220,  500 => 219,  490 => 214,  486 => 213,  480 => 210,  475 => 207,  472 => 206,  462 => 201,  458 => 200,  452 => 197,  447 => 194,  445 => 193,  438 => 189,  433 => 186,  423 => 181,  419 => 180,  413 => 177,  408 => 174,  405 => 173,  395 => 168,  391 => 167,  385 => 164,  380 => 161,  377 => 160,  367 => 155,  363 => 154,  357 => 151,  352 => 148,  349 => 147,  339 => 142,  335 => 141,  329 => 138,  324 => 135,  321 => 134,  313 => 129,  307 => 126,  302 => 123,  299 => 122,  289 => 117,  285 => 116,  279 => 113,  274 => 110,  271 => 109,  263 => 104,  257 => 101,  252 => 98,  249 => 97,  239 => 92,  235 => 91,  229 => 88,  224 => 85,  222 => 84,  215 => 80,  202 => 72,  196 => 69,  182 => 60,  176 => 57,  168 => 52,  161 => 48,  156 => 45,  148 => 43,  140 => 41,  137 => 40,  129 => 38,  121 => 36,  118 => 35,  110 => 33,  102 => 31,  100 => 30,  92 => 24,  84 => 19,  79 => 16,  77 => 15,  73 => 13,  62 => 11,  58 => 10,  53 => 8,  46 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "extension/module/reports.twig", "");
    }
}
