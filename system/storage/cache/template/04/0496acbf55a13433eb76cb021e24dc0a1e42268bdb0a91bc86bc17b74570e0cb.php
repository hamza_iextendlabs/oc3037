<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* extension/module/reports.twig */
class __TwigTemplate_6db8a6afb70bda32f7335e99a30031da40783a01d3f3e1a226c57b365898b7a2 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
    <div class=\"page-header\">
        <div class=\"container-fluid\">
        <div class=\"pull-right\">
            <a href=\"";
        // line 6
        echo ($context["report_setting"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["text_setting"] ?? null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-cog fw\"></i></a>
        </div>
            <h1>";
        // line 8
        echo ($context["heading_title"] ?? null);
        echo "</h1>
            <ul class=\"breadcrumb\">
                ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 11
            echo "                <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 11);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 11);
            echo "</a></li>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "            </ul>
        </div>
        ";
        // line 15
        if (($context["isFirstLoad"] ?? null)) {
            // line 16
            echo "            <div class=\"col-md-12\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid#4cb64c; border-radius: 15px; margin-left: 10px;\">
                    <div class=\"text-success text-center mt-3 panel-body\">
                        <h4><b>";
            // line 19
            echo ($context["isFirstLoad"] ?? null);
            echo "</b></h4>
                    </div>
                </div>
            </div>
            ";
        }
        // line 24
        echo "    </div>
    <div class=\"jumbotron\">
        <div class=\"panel panel-default\">
            <div class=\"panel-heading\">
            <div class=\"pull-right\"><a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"><h2><i class=\"fa fa-calendar\"></i> <i class=\"caret\"></h2></i></a>
                <ul id=\"range\" class=\"dropdown-menu dropdown-menu-right\">
                    ";
        // line 30
        if ((($context["text_sales_report"] ?? null) == "Daily Sales Report")) {
            // line 31
            echo "                  <li id=\"day\" class=\"active\"><a>Today</a></li> <input type=\"hidden\" value=\"";
            echo ($context["day_start"] ?? null);
            echo "\" name=\"day_start\"> <input type=\"hidden\" value=\"";
            echo ($context["day_end"] ?? null);
            echo "\" name=\"day_end\">
                    ";
        } else {
            // line 33
            echo "                  <li id=\"day\"><a>Today</a></li> <input type=\"hidden\" value=\"";
            echo ($context["day_start"] ?? null);
            echo "\" name=\"day_start\"> <input type=\"hidden\" value=\"";
            echo ($context["day_end"] ?? null);
            echo "\" name=\"day_end\">
                    ";
        }
        // line 35
        echo "                    ";
        if ((($context["text_sales_report"] ?? null) == "Monthly Sales Report")) {
            // line 36
            echo "                  <li id=\"month\" class=\"active\"><a>Month</a></li> <input type=\"hidden\" value=\"";
            echo ($context["month_start"] ?? null);
            echo "\" name=\"month_start\"> <input type=\"hidden\" value=\"";
            echo ($context["month_end"] ?? null);
            echo "\" name=\"month_end\">
                    ";
        } else {
            // line 38
            echo "                  <li id=\"month\"><a>Month</a></li> <input type=\"hidden\" value=\"";
            echo ($context["month_start"] ?? null);
            echo "\" name=\"month_start\"> <input type=\"hidden\" value=\"";
            echo ($context["month_end"] ?? null);
            echo "\" name=\"month_end\">
                    ";
        }
        // line 40
        echo "                    ";
        if ((($context["text_sales_report"] ?? null) == "Yearly Sales Report")) {
            // line 41
            echo "                  <li id=\"year\" class=\"active\"><a>Year</a></li> <input type=\"hidden\" value=\"";
            echo ($context["year_start"] ?? null);
            echo "\" name=\"year_start\"> <input type=\"hidden\" value=\"";
            echo ($context["year_end"] ?? null);
            echo "\" name=\"year_end\">
                    ";
        } else {
            // line 43
            echo "                  <li id=\"year\"><a>Year</a></li> <input type=\"hidden\" value=\"";
            echo ($context["year_start"] ?? null);
            echo "\" name=\"year_start\"> <input type=\"hidden\" value=\"";
            echo ($context["year_end"] ?? null);
            echo "\" name=\"year_end\">
                    ";
        }
        // line 45
        echo "                </ul>
                <ul id=\"range\" class=\"dropdown-menu dropdown-menu-right\">
                    <li class=\"active\"><a>Today</a>
                </ul>
              </div>
            <h2 class=\"panel-tital\">";
        // line 50
        echo ($context["text_sales_report"] ?? null);
        echo "</h2>
            </div>
        </div>
        <div class=\"row w-100 container-fluid\">
            ";
        // line 54
        if ((($context["order_status"] ?? null) == "on")) {
            // line 55
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid#f3a638; border-radius: 15px; height: 185px;\">
                    <div class=\"text-warning text-center mt-3 panel-body\">
                        <h4>";
            // line 58
            echo ($context["text_total_order"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-warning text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 61
            echo ($context["total_order"] ?? null);
            echo "</h1>
                        <a href=\"";
            // line 62
            echo ($context["href_order"] ?? null);
            echo "\"> <h5 class=\"text-warning\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 67
        echo "            ";
        if ((($context["sales_status"] ?? null) == "on")) {
            // line 68
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid #4cb657; border-radius: 15px; height: 185px;\">
                    <div class=\"text-success text-center mt-3 panel-body\">
                        <h4>";
            // line 71
            echo ($context["text_sales"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-success text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 74
            echo ($context["sales"] ?? null);
            echo "</h1>
                    </div>
                </div>
            </div>
            ";
        }
        // line 79
        echo "            ";
        if ((($context["return_status"] ?? null) == "on")) {
            // line 80
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid #e3503e; border-radius: 15px; height: 185px;\">
                    <div class=\"text-danger text-center mt-3 panel-body\">
                        <h4>";
            // line 83
            echo ($context["text_return"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-danger text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 86
            echo ($context["return"] ?? null);
            echo "</h1>
                        <a href=\"";
            // line 87
            echo ($context["href_refunded"] ?? null);
            echo "\"> <h5 class=\"text-danger\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 92
        echo "            ";
        if ((($context["tax_status"] ?? null) == "on")) {
            // line 93
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid #31708f; border-radius: 15px; height: 185px;\">
                    <div class=\"text-info text-center mt-3 panel-body\">
                        <h4>";
            // line 96
            echo ($context["text_tax"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-info text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 99
            echo ($context["tax"] ?? null);
            echo "</h1>
                    </div>
                </div>
            </div>
            ";
        }
        // line 104
        echo "            ";
        if ((($context["complete_order_status"] ?? null) == "on")) {
            // line 105
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid#f3a638; border-radius: 15px; height: 185px;\">
                    <div class=\"text-warning text-center mt-3 panel-body\">
                        <h4>";
            // line 108
            echo ($context["text_complete_order"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-warning text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 111
            echo ($context["total_complete_order"] ?? null);
            echo "</h1>
                        <a href=\"";
            // line 112
            echo ($context["href_complete"] ?? null);
            echo "\"> <h5 class=\"text-warning\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 117
        echo "            ";
        if ((($context["pending_order_status"] ?? null) == "on")) {
            // line 118
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid #4cb657; border-radius: 15px; height: 185px;\">
                    <div class=\"text-success text-center mt-3 panel-body\">
                        <h4>";
            // line 121
            echo ($context["text_pending_order"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-success text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 124
            echo ($context["total_panding_order"] ?? null);
            echo "</h1>
                        <a href=\"";
            // line 125
            echo ($context["href_panding"] ?? null);
            echo "\"> <h5 class=\"text-success\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 130
        echo "            ";
        if ((($context["canceled_order_status"] ?? null) == "on")) {
            // line 131
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid #e3503e; border-radius: 15px; height: 185px;\">
                    <div class=\"text-danger text-center mt-3 panel-body\">
                        <h4>";
            // line 134
            echo ($context["text_canceled_order"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-danger text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 137
            echo ($context["total_canceled_order"] ?? null);
            echo "</h1>
                        <a href=\"";
            // line 138
            echo ($context["href_canceled"] ?? null);
            echo "\"> <h5 class=\"text-danger\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 143
        echo "            ";
        if ((($context["refunded_order_status"] ?? null) == "on")) {
            // line 144
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid #31708f; border-radius: 15px; height: 185px;\">
                    <div class=\"text-info text-center mt-3 panel-body\">
                        <h4>";
            // line 147
            echo ($context["text_refunded_order"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-info text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 150
            echo ($context["total_refunded_order"] ?? null);
            echo "</h1>
                        <a href=\"";
            // line 151
            echo ($context["href_refunded"] ?? null);
            echo "\"> <h5 class=\"text-info\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 156
        echo "        </div>
        <div class=\"panel panel-default\">
            <div class=\"panel-heading\">
                <h2 class=\"panel-tital\">";
        // line 159
        echo ($context["text_product_report"] ?? null);
        echo "</h2>
            </div>
        </div>
        <div class=\"row w-100 container-fluid\">
            ";
        // line 163
        if ((($context["product_status"] ?? null) == "on")) {
            // line 164
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid#f3a638; border-radius: 15px; height: 185px;\">
                    <div class=\"text-warning text-center mt-3 panel-body\">
                        <h4>";
            // line 167
            echo ($context["text_total_product"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-warning text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 170
            echo ($context["total_product"] ?? null);
            echo "</h1>
                        <a href=\"";
            // line 171
            echo ($context["href_product"] ?? null);
            echo "\"> <h5 class=\"text-warning\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 176
        echo "            ";
        if ((($context["outofstock_product_status"] ?? null) == "on")) {
            // line 177
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid #4cb657; border-radius: 15px; height: 185px;\">
                    <div class=\"text-success text-center mt-3 panel-body\">
                        <h4>";
            // line 180
            echo ($context["text_total_outOfStock_product"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-success text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 183
            echo ($context["total_OutOfStock_product"] ?? null);
            echo "</h1>
                        <a href=\"";
            // line 184
            echo ($context["href_outofstock_product"] ?? null);
            echo "\"> <h5 class=\"text-success\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 189
        echo "            ";
        if ((($context["lowestselling_product_status"] ?? null) == "on")) {
            // line 190
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\"
                    style=\"border: 1px solid #e3503e; border-radius: 15px; height: 185px;\">
                    <div class=\"text-danger text-center mt-3 panel-body\">
                        <h4>";
            // line 194
            echo ($context["text_lowest_selling_product"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-danger text-center mt-2 panel-body\">
                        <h2><b>";
            // line 197
            echo ($context["total_LowestSelling_product"] ?? null);
            echo "</b></h2>
                        <a href=\"";
            // line 198
            echo ($context["href_lowestselling_product"] ?? null);
            echo "\"> <h5 class=\"text-danger\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 203
        echo "            ";
        if ((($context["highestselling_product_status"] ?? null) == "on")) {
            // line 204
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\"
                    style=\"border: 1px solid #31708f; border-radius: 15px; height: 185px;\">
                    <div class=\"text-info text-center mt-3 panel-body\">
                        <h4>";
            // line 208
            echo ($context["text_highest_selling_product"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-info text-center mt-2 panel-body\">
                        <h2><b>";
            // line 211
            echo ($context["total_HighestSelling_product"] ?? null);
            echo "</b></h2>
                        <a href=\"";
            // line 212
            echo ($context["href_highestselling_product"] ?? null);
            echo "\"> <h5 class=\"text-info\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 217
        echo "            ";
        if ((($context["topview_product_status"] ?? null) == "on")) {
            // line 218
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\"
                    style=\"border: 1px solid#f3a638; border-radius: 15px; height: 185px;\">
                    <div class=\"text-warning text-center mt-3 panel-body\">
                        <h4>";
            // line 222
            echo ($context["text_TopView_product"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-warning text-center mt-2 panel-body\">
                        <h2><b>";
            // line 225
            echo ($context["total_TopView_product"] ?? null);
            echo "</b></h2>
                        <a href=\"";
            // line 226
            echo ($context["href_topView_product"] ?? null);
            echo "\"> <h5 class=\"text-warning\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 231
        echo "            ";
        if ((($context["lowestview_product_status"] ?? null) == "on")) {
            // line 232
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\"
                    style=\"border: 1px solid #4cb657; border-radius: 15px; height: 185px;\">
                    <div class=\"text-success text-center mt-3 panel-body\">
                        <h4>";
            // line 236
            echo ($context["text_LowestView_product"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-success text-center mt-2 panel-body\">
                        <h2><b>";
            // line 239
            echo ($context["total_LowestView_product"] ?? null);
            echo "</b></h2>
                        <a href=\"";
            // line 240
            echo ($context["href_lowestview_product"] ?? null);
            echo "\"> <h5 class=\"text-success\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 245
        echo "        </div>
        <div class=\"panel panel-default\">
            <div class=\"panel-heading\">
                <h2 class=\"panel-tital\">";
        // line 248
        echo ($context["text_category_report"] ?? null);
        echo "</h2>
            </div>
        </div>
        <div class=\"row w-100 container-fluid\">
            ";
        // line 252
        if ((($context["customers_status"] ?? null) == "on")) {
            // line 253
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid#f3a638; border-radius: 15px; height: 185px;\">
                    <div class=\"text-warning text-center mt-3 panel-body\">
                        <h4>";
            // line 256
            echo ($context["text_total_customer"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-warning text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 259
            echo ($context["total_customer"] ?? null);
            echo "</h1>
                        <a href=\"";
            // line 260
            echo ($context["href_customer"] ?? null);
            echo "\"> <h5 class=\"text-warning\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 265
        echo "            ";
        if ((($context["category_status"] ?? null) == "on")) {
            // line 266
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid #4cb657; border-radius: 15px; height: 185px;\">
                    <div class=\"text-success text-center mt-3 panel-body\">
                        <h4>";
            // line 269
            echo ($context["text_total_category"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-success text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 272
            echo ($context["total_category"] ?? null);
            echo "</h1>
                        <a href=\"";
            // line 273
            echo ($context["href_category"] ?? null);
            echo "\"> <h5 class=\"text-success\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 278
        echo "            ";
        if ((($context["enable_category_status"] ?? null) == "on")) {
            // line 279
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid #e3503e; border-radius: 15px; height: 185px;\">
                    <div class=\"text-danger text-center mt-3 panel-body\">
                        <h4>";
            // line 282
            echo ($context["text_total_enable_category"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-danger text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 285
            echo ($context["total_enable_category"] ?? null);
            echo "</h1>
                    </div>
                </div>
            </div>
            ";
        }
        // line 290
        echo "            ";
        if ((($context["disable_category_status"] ?? null) == "on")) {
            // line 291
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid #31708f; border-radius: 15px; height: 185px;\">
                    <div class=\"text-info text-center mt-3 panel-body\">
                        <h4>";
            // line 294
            echo ($context["text_total_disable_category"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-info text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 297
            echo ($context["total_disable_category"] ?? null);
            echo "</h1>
                    </div>
                </div>
            </div>
            ";
        }
        // line 302
        echo "            ";
        if ((($context["nanufacturer_status"] ?? null) == "on")) {
            // line 303
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid#f3a638; border-radius: 15px; height: 185px;\">
                    <div class=\"text-warning text-center mt-3 panel-body\">
                        <h4>";
            // line 306
            echo ($context["text_total_manufacturer"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-warning text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 309
            echo ($context["total_manufacturer"] ?? null);
            echo "</h1>
                    </div>
                </div>
            </div>
            ";
        }
        // line 314
        echo "            ";
        if ((($context["topbrand_status"] ?? null) == "on")) {
            // line 315
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\"
                    style=\"border: 1px solid #4cb657; border-radius: 15px; height: 185px;\">
                    <div class=\"text-success text-center mt-3 panel-body\">
                        <h4>";
            // line 319
            echo ($context["text_top_brand"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-success text-center mt-2 panel-body\">
                        <h2><b>";
            // line 322
            echo ($context["top_brand"] ?? null);
            echo "</b></h2>
                    </div>
                </div>
            </div>
            ";
        }
        // line 327
        echo "        </div>
    </div>
</div>
</div>
<script type=\"text/javascript\"><!--
    \$('#day').on('click', function() {
        
        var url = '';
    
      var day_start = \$('input[name=\\'day_start\\']').val();
    
        if (day_start) {
            url += '&day_start=' + (day_start);
        }
      
      var day_end = \$('input[name=\\'day_end\\']').val();
    
        if (day_end) {
            url += '&day_end=' + (day_end);
        }
    
          location = 'index.php?route=extension/module/reports/info&user_token=";
        // line 348
        echo ($context["user_token"] ?? null);
        echo "' + url;
    });
    //--></script>

<script type=\"text/javascript\"><!--
    \$('#month').on('click', function() {
        var url = '';
    
      var month_start = \$('input[name=\\'month_start\\']').val();
    
        if (month_start) {
            url += '&month_start=' + encodeURIComponent(month_start);
        }
      
      var month_end = \$('input[name=\\'month_end\\']').val();
    
        if (month_end) {
            url += '&month_end=' + encodeURIComponent(month_end);
        }
    
          location = 'index.php?route=extension/module/reports/info&user_token=";
        // line 368
        echo ($context["user_token"] ?? null);
        echo "' + url;
    });
    //--></script>

<script type=\"text/javascript\"><!--
    \$('#year').on('click', function() {
        var url = '';
    
      var year_start = \$('input[name=\\'year_start\\']').val();
    
        if (year_start) {
            url += '&year_start=' + encodeURIComponent(year_start);
        }
      
      var year_end = \$('input[name=\\'year_end\\']').val();
    
        if (year_end) {
            url += '&year_end=' + encodeURIComponent(year_end);
        }
    
          location = 'index.php?route=extension/module/reports/info&user_token=";
        // line 388
        echo ($context["user_token"] ?? null);
        echo "' + url;
    });
    //--></script>
";
        // line 391
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "extension/module/reports.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  795 => 391,  789 => 388,  766 => 368,  743 => 348,  720 => 327,  712 => 322,  706 => 319,  700 => 315,  697 => 314,  689 => 309,  683 => 306,  678 => 303,  675 => 302,  667 => 297,  661 => 294,  656 => 291,  653 => 290,  645 => 285,  639 => 282,  634 => 279,  631 => 278,  621 => 273,  617 => 272,  611 => 269,  606 => 266,  603 => 265,  593 => 260,  589 => 259,  583 => 256,  578 => 253,  576 => 252,  569 => 248,  564 => 245,  554 => 240,  550 => 239,  544 => 236,  538 => 232,  535 => 231,  525 => 226,  521 => 225,  515 => 222,  509 => 218,  506 => 217,  496 => 212,  492 => 211,  486 => 208,  480 => 204,  477 => 203,  467 => 198,  463 => 197,  457 => 194,  451 => 190,  448 => 189,  438 => 184,  434 => 183,  428 => 180,  423 => 177,  420 => 176,  410 => 171,  406 => 170,  400 => 167,  395 => 164,  393 => 163,  386 => 159,  381 => 156,  371 => 151,  367 => 150,  361 => 147,  356 => 144,  353 => 143,  343 => 138,  339 => 137,  333 => 134,  328 => 131,  325 => 130,  315 => 125,  311 => 124,  305 => 121,  300 => 118,  297 => 117,  287 => 112,  283 => 111,  277 => 108,  272 => 105,  269 => 104,  261 => 99,  255 => 96,  250 => 93,  247 => 92,  237 => 87,  233 => 86,  227 => 83,  222 => 80,  219 => 79,  211 => 74,  205 => 71,  200 => 68,  197 => 67,  187 => 62,  183 => 61,  177 => 58,  172 => 55,  170 => 54,  163 => 50,  156 => 45,  148 => 43,  140 => 41,  137 => 40,  129 => 38,  121 => 36,  118 => 35,  110 => 33,  102 => 31,  100 => 30,  92 => 24,  84 => 19,  79 => 16,  77 => 15,  73 => 13,  62 => 11,  58 => 10,  53 => 8,  46 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "extension/module/reports.twig", "");
    }
}
