<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* extension/module/reports.twig */
class __TwigTemplate_ce01107b068dec639d175f7d53be0ae48574cb746d5414eeebe3b8f4011b5685 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
    <div class=\"page-header\">
        <div class=\"container-fluid\">
        <div class=\"pull-right\">
            <a href=\"";
        // line 6
        echo ($context["report_setting"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["text_setting"] ?? null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-cog fw\"></i></a>
        </div>
            <h1>";
        // line 8
        echo ($context["heading_title"] ?? null);
        echo "</h1>
            <ul class=\"breadcrumb\">
                ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 11
            echo "                <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 11);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 11);
            echo "</a></li>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "            </ul>
        </div>
        ";
        // line 15
        if (($context["isFirstLoad"] ?? null)) {
            // line 16
            echo "            <div class=\"col-md-12\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid#4cb64c; border-radius: 15px; margin-left: 10px;\">
                    <div class=\"text-success text-center mt-3 panel-body\">
                        <h4><b>";
            // line 19
            echo ($context["isFirstLoad"] ?? null);
            echo "</b></h4>
                    </div>
                </div>
            </div>
            ";
        }
        // line 24
        echo "    </div>
    <div class=\"jumbotron\">
        <div class=\"panel panel-default\">
            <div class=\"panel-heading\">
            <div class=\"pull-right\"><a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"><h2><i class=\"fa fa-calendar\"></i> <i class=\"caret\"></h2></i></a>
                <ul id=\"range\" class=\"dropdown-menu dropdown-menu-right\">
                    ";
        // line 30
        if ((($context["text_sales_report"] ?? null) == "Daily Sales Report")) {
            // line 31
            echo "                  <li id=\"day\" class=\"active\"><a>Today</a></li> <input type=\"hidden\" value=\"";
            echo ($context["day_start"] ?? null);
            echo "\" name=\"day_start\"> <input type=\"hidden\" value=\"";
            echo ($context["day_end"] ?? null);
            echo "\" name=\"day_end\">
                    ";
        } else {
            // line 33
            echo "                  <li id=\"day\"><a>Today</a></li> <input type=\"hidden\" value=\"";
            echo ($context["day_start"] ?? null);
            echo "\" name=\"day_start\"> <input type=\"hidden\" value=\"";
            echo ($context["day_end"] ?? null);
            echo "\" name=\"day_end\">
                    ";
        }
        // line 35
        echo "                    ";
        if ((($context["text_sales_report"] ?? null) == "Monthly Sales Report")) {
            // line 36
            echo "                  <li id=\"month\" class=\"active\"><a>Month</a></li> <input type=\"hidden\" value=\"";
            echo ($context["month_start"] ?? null);
            echo "\" name=\"month_start\"> <input type=\"hidden\" value=\"";
            echo ($context["month_end"] ?? null);
            echo "\" name=\"month_end\">
                    ";
        } else {
            // line 38
            echo "                  <li id=\"month\"><a>Month</a></li> <input type=\"hidden\" value=\"";
            echo ($context["month_start"] ?? null);
            echo "\" name=\"month_start\"> <input type=\"hidden\" value=\"";
            echo ($context["month_end"] ?? null);
            echo "\" name=\"month_end\">
                    ";
        }
        // line 40
        echo "                    ";
        if ((($context["text_sales_report"] ?? null) == "Yearly Sales Report")) {
            // line 41
            echo "                  <li id=\"year\" class=\"active\"><a>Year</a></li> <input type=\"hidden\" value=\"";
            echo ($context["year_start"] ?? null);
            echo "\" name=\"year_start\"> <input type=\"hidden\" value=\"";
            echo ($context["year_end"] ?? null);
            echo "\" name=\"year_end\">
                    ";
        } else {
            // line 43
            echo "                  <li id=\"year\"><a>Year</a></li> <input type=\"hidden\" value=\"";
            echo ($context["year_start"] ?? null);
            echo "\" name=\"year_start\"> <input type=\"hidden\" value=\"";
            echo ($context["year_end"] ?? null);
            echo "\" name=\"year_end\">
                    ";
        }
        // line 45
        echo "                
                </ul>
              </div>
            <h2 class=\"panel-tital\">";
        // line 48
        echo ($context["text_sales_report"] ?? null);
        echo "</h2>
            </div>
        </div>
                <h3 class=\"panel-title\" style=\"color:DodgerBlue;\"> <i class=\"fa fa-filter\"></i>";
        // line 51
        echo ($context["custom_filter"] ?? null);
        echo "</h3>
              <div class=\"panel-body\">
                <div class=\"container-fluid\">
                  <div class=\"form-group\">
                    <label for=\"input-date-start\" class=\"col-sm-6 control-label\">";
        // line 55
        echo ($context["custom_date_start"] ?? null);
        echo "</label>
                    <div class=\"col-sm-12\">
                      <div class=\"input-group datetime\">
                        <input type=\"text\" name=\"start\" value=\"";
        // line 58
        echo ($context["start"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["custom_date_start"] ?? null);
        echo "\" data-date-format=\"YYYY-MM-DD HH:mm:ss\" id=\"input-date-start\" class=\"form-control\" class=\"col-sm-2\"/> <span class=\"input-group-btn\">
                        <button class=\"btn btn-default\" type=\"button\"><i class=\"fa fa-calendar\"></i></button>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class=\"container-fluid\">
                  <div class=\"form-group\">
                    <label for=\"input-date-end\" class=\"col-sm-6 control-label\">";
        // line 67
        echo ($context["custom_date_end"] ?? null);
        echo "</label>
                    <div class=\"col-sm-12\">
                      <div class=\"input-group datetime\">
                        <input type=\"text\" name=\"end\" value=\"";
        // line 70
        echo ($context["end"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["custom_date_end"] ?? null);
        echo "\" data-date-format=\"YYYY-MM-DD HH:mm:ss\" id=\"input-date-end\" class=\"form-control\" class=\"col-sm-2\"/> <span class=\"input-group-btn\">
                        <button class=\"btn btn-default\" type=\"button\"><i class=\"fa fa-calendar\"></i></button>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class=\"form-group text-right\">
                  <button type=\"button\" id=\"button-custom\" class=\"btn btn-default\"><i class=\"fa fa-filter\"></i> ";
        // line 78
        echo ($context["button_filter"] ?? null);
        echo " </button>
                </div>
        </div>
        <div class=\"row w-100 container-fluid\">
            ";
        // line 82
        if ((($context["order_status"] ?? null) == "on")) {
            // line 83
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid#f3a638; border-radius: 15px; height: 185px;\">
                    <div class=\"text-warning text-center mt-3 panel-body\">
                        <h4>";
            // line 86
            echo ($context["text_total_order"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-warning text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 89
            echo ($context["total_order"] ?? null);
            echo "</h1>
                        <a href=\"";
            // line 90
            echo ($context["href_order"] ?? null);
            echo "\"> <h5 class=\"text-warning\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 95
        echo "            ";
        if ((($context["sales_status"] ?? null) == "on")) {
            // line 96
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid #4cb657; border-radius: 15px; height: 185px;\">
                    <div class=\"text-success text-center mt-3 panel-body\">
                        <h4>";
            // line 99
            echo ($context["text_sales"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-success text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 102
            echo ($context["sales"] ?? null);
            echo "</h1>
                    </div>
                </div>
            </div>
            ";
        }
        // line 107
        echo "            ";
        if ((($context["return_status"] ?? null) == "on")) {
            // line 108
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid #e3503e; border-radius: 15px; height: 185px;\">
                    <div class=\"text-danger text-center mt-3 panel-body\">
                        <h4>";
            // line 111
            echo ($context["text_return"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-danger text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 114
            echo ($context["return"] ?? null);
            echo "</h1>
                        <a href=\"";
            // line 115
            echo ($context["href_refunded"] ?? null);
            echo "\"> <h5 class=\"text-danger\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 120
        echo "            ";
        if ((($context["tax_status"] ?? null) == "on")) {
            // line 121
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid #31708f; border-radius: 15px; height: 185px;\">
                    <div class=\"text-info text-center mt-3 panel-body\">
                        <h4>";
            // line 124
            echo ($context["text_tax"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-info text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 127
            echo ($context["tax"] ?? null);
            echo "</h1>
                    </div>
                </div>
            </div>
            ";
        }
        // line 132
        echo "            ";
        if ((($context["complete_order_status"] ?? null) == "on")) {
            // line 133
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid#f3a638; border-radius: 15px; height: 185px;\">
                    <div class=\"text-warning text-center mt-3 panel-body\">
                        <h4>";
            // line 136
            echo ($context["text_complete_order"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-warning text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 139
            echo ($context["total_complete_order"] ?? null);
            echo "</h1>
                        <a href=\"";
            // line 140
            echo ($context["href_complete"] ?? null);
            echo "\"> <h5 class=\"text-warning\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 145
        echo "            ";
        if ((($context["pending_order_status"] ?? null) == "on")) {
            // line 146
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid #4cb657; border-radius: 15px; height: 185px;\">
                    <div class=\"text-success text-center mt-3 panel-body\">
                        <h4>";
            // line 149
            echo ($context["text_pending_order"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-success text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 152
            echo ($context["total_panding_order"] ?? null);
            echo "</h1>
                        <a href=\"";
            // line 153
            echo ($context["href_panding"] ?? null);
            echo "\"> <h5 class=\"text-success\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 158
        echo "            ";
        if ((($context["canceled_order_status"] ?? null) == "on")) {
            // line 159
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid #e3503e; border-radius: 15px; height: 185px;\">
                    <div class=\"text-danger text-center mt-3 panel-body\">
                        <h4>";
            // line 162
            echo ($context["text_canceled_order"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-danger text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 165
            echo ($context["total_canceled_order"] ?? null);
            echo "</h1>
                        <a href=\"";
            // line 166
            echo ($context["href_canceled"] ?? null);
            echo "\"> <h5 class=\"text-danger\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 171
        echo "            ";
        if ((($context["refunded_order_status"] ?? null) == "on")) {
            // line 172
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid #31708f; border-radius: 15px; height: 185px;\">
                    <div class=\"text-info text-center mt-3 panel-body\">
                        <h4>";
            // line 175
            echo ($context["text_refunded_order"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-info text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 178
            echo ($context["total_refunded_order"] ?? null);
            echo "</h1>
                        <a href=\"";
            // line 179
            echo ($context["href_refunded"] ?? null);
            echo "\"> <h5 class=\"text-info\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 184
        echo "        </div>
        <div class=\"panel panel-default\">
            <div class=\"panel-heading\">
                <h2 class=\"panel-tital\">";
        // line 187
        echo ($context["text_product_report"] ?? null);
        echo "</h2>
            </div>
        </div>
        <div class=\"row w-100 container-fluid\">
            ";
        // line 191
        if ((($context["product_status"] ?? null) == "on")) {
            // line 192
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid#f3a638; border-radius: 15px; height: 185px;\">
                    <div class=\"text-warning text-center mt-3 panel-body\">
                        <h4>";
            // line 195
            echo ($context["text_total_product"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-warning text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 198
            echo ($context["total_product"] ?? null);
            echo "</h1>
                        <a href=\"";
            // line 199
            echo ($context["href_product"] ?? null);
            echo "\"> <h5 class=\"text-warning\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 204
        echo "            ";
        if ((($context["outofstock_product_status"] ?? null) == "on")) {
            // line 205
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid #4cb657; border-radius: 15px; height: 185px;\">
                    <div class=\"text-success text-center mt-3 panel-body\">
                        <h4>";
            // line 208
            echo ($context["text_total_outOfStock_product"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-success text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 211
            echo ($context["total_OutOfStock_product"] ?? null);
            echo "</h1>
                        <a href=\"";
            // line 212
            echo ($context["href_outofstock_product"] ?? null);
            echo "\"> <h5 class=\"text-success\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 217
        echo "            ";
        if ((($context["lowestselling_product_status"] ?? null) == "on")) {
            // line 218
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\"
                    style=\"border: 1px solid #e3503e; border-radius: 15px; height: 185px;\">
                    <div class=\"text-danger text-center mt-3 panel-body\">
                        <h4>";
            // line 222
            echo ($context["text_lowest_selling_product"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-danger text-center mt-2 panel-body\">
                        <h2><b>";
            // line 225
            echo ($context["total_LowestSelling_product"] ?? null);
            echo "</b></h2>
                        <a href=\"";
            // line 226
            echo ($context["href_lowestselling_product"] ?? null);
            echo "\"> <h5 class=\"text-danger\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 231
        echo "            ";
        if ((($context["highestselling_product_status"] ?? null) == "on")) {
            // line 232
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\"
                    style=\"border: 1px solid #31708f; border-radius: 15px; height: 185px;\">
                    <div class=\"text-info text-center mt-3 panel-body\">
                        <h4>";
            // line 236
            echo ($context["text_highest_selling_product"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-info text-center mt-2 panel-body\">
                        <h2><b>";
            // line 239
            echo ($context["total_HighestSelling_product"] ?? null);
            echo "</b></h2>
                        <a href=\"";
            // line 240
            echo ($context["href_highestselling_product"] ?? null);
            echo "\"> <h5 class=\"text-info\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 245
        echo "            ";
        if ((($context["topview_product_status"] ?? null) == "on")) {
            // line 246
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\"
                    style=\"border: 1px solid#f3a638; border-radius: 15px; height: 185px;\">
                    <div class=\"text-warning text-center mt-3 panel-body\">
                        <h4>";
            // line 250
            echo ($context["text_TopView_product"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-warning text-center mt-2 panel-body\">
                        <h2><b>";
            // line 253
            echo ($context["total_TopView_product"] ?? null);
            echo "</b></h2>
                        <a href=\"";
            // line 254
            echo ($context["href_topView_product"] ?? null);
            echo "\"> <h5 class=\"text-warning\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 259
        echo "            ";
        if ((($context["lowestview_product_status"] ?? null) == "on")) {
            // line 260
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\"
                    style=\"border: 1px solid #4cb657; border-radius: 15px; height: 185px;\">
                    <div class=\"text-success text-center mt-3 panel-body\">
                        <h4>";
            // line 264
            echo ($context["text_LowestView_product"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-success text-center mt-2 panel-body\">
                        <h2><b>";
            // line 267
            echo ($context["total_LowestView_product"] ?? null);
            echo "</b></h2>
                        <a href=\"";
            // line 268
            echo ($context["href_lowestview_product"] ?? null);
            echo "\"> <h5 class=\"text-success\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 273
        echo "        </div>
        <div class=\"panel panel-default\">
            <div class=\"panel-heading\">
                <h2 class=\"panel-tital\">";
        // line 276
        echo ($context["text_category_report"] ?? null);
        echo "</h2>
            </div>
        </div>
        <div class=\"row w-100 container-fluid\">
            ";
        // line 280
        if ((($context["customers_status"] ?? null) == "on")) {
            // line 281
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid#f3a638; border-radius: 15px; height: 185px;\">
                    <div class=\"text-warning text-center mt-3 panel-body\">
                        <h4>";
            // line 284
            echo ($context["text_total_customer"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-warning text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 287
            echo ($context["total_customer"] ?? null);
            echo "</h1>
                        <a href=\"";
            // line 288
            echo ($context["href_customer"] ?? null);
            echo "\"> <h5 class=\"text-warning\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 293
        echo "            ";
        if ((($context["category_status"] ?? null) == "on")) {
            // line 294
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid #4cb657; border-radius: 15px; height: 185px;\">
                    <div class=\"text-success text-center mt-3 panel-body\">
                        <h4>";
            // line 297
            echo ($context["text_total_category"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-success text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 300
            echo ($context["total_category"] ?? null);
            echo "</h1>
                        <a href=\"";
            // line 301
            echo ($context["href_category"] ?? null);
            echo "\"> <h5 class=\"text-success\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 306
        echo "            ";
        if ((($context["enable_category_status"] ?? null) == "on")) {
            // line 307
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid #e3503e; border-radius: 15px; height: 185px;\">
                    <div class=\"text-danger text-center mt-3 panel-body\">
                        <h4>";
            // line 310
            echo ($context["text_total_enable_category"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-danger text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 313
            echo ($context["total_enable_category"] ?? null);
            echo "</h1>
                    </div>
                </div>
            </div>
            ";
        }
        // line 318
        echo "            ";
        if ((($context["disable_category_status"] ?? null) == "on")) {
            // line 319
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid #31708f; border-radius: 15px; height: 185px;\">
                    <div class=\"text-info text-center mt-3 panel-body\">
                        <h4>";
            // line 322
            echo ($context["text_total_disable_category"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-info text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 325
            echo ($context["total_disable_category"] ?? null);
            echo "</h1>
                    </div>
                </div>
            </div>
            ";
        }
        // line 330
        echo "            ";
        if ((($context["nanufacturer_status"] ?? null) == "on")) {
            // line 331
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid#f3a638; border-radius: 15px; height: 185px;\">
                    <div class=\"text-warning text-center mt-3 panel-body\">
                        <h4>";
            // line 334
            echo ($context["text_total_manufacturer"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-warning text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 337
            echo ($context["total_manufacturer"] ?? null);
            echo "</h1>
                    </div>
                </div>
            </div>
            ";
        }
        // line 342
        echo "            ";
        if ((($context["topbrand_status"] ?? null) == "on")) {
            // line 343
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\"
                    style=\"border: 1px solid #4cb657; border-radius: 15px; height: 185px;\">
                    <div class=\"text-success text-center mt-3 panel-body\">
                        <h4>";
            // line 347
            echo ($context["text_top_brand"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-success text-center mt-2 panel-body\">
                        <h2><b>";
            // line 350
            echo ($context["top_brand"] ?? null);
            echo "</b></h2>
                    </div>
                </div>
            </div>
            ";
        }
        // line 355
        echo "        </div>
    </div>
</div>
</div>
<script type=\"text/javascript\"><!--
    \$('#day').on('click', function() {
        
        var url = '';
    
      var day_start = \$('input[name=\\'day_start\\']').val();
    
        if (day_start) {
            url += '&day_start=' + (day_start);
        }
      
      var day_end = \$('input[name=\\'day_end\\']').val();
    
        if (day_end) {
            url += '&day_end=' + (day_end);
        }
    
          location = 'index.php?route=extension/module/reports/info&user_token=";
        // line 376
        echo ($context["user_token"] ?? null);
        echo "' + url;
    });
    //--></script>

<script type=\"text/javascript\"><!--
    \$('#month').on('click', function() {
        var url = '';
    
      var month_start = \$('input[name=\\'month_start\\']').val();
    
        if (month_start) {
            url += '&month_start=' + encodeURIComponent(month_start);
        }
      
      var month_end = \$('input[name=\\'month_end\\']').val();
    
        if (month_end) {
            url += '&month_end=' + encodeURIComponent(month_end);
        }
    
          location = 'index.php?route=extension/module/reports/info&user_token=";
        // line 396
        echo ($context["user_token"] ?? null);
        echo "' + url;
    });
    //--></script>

<script type=\"text/javascript\"><!--
    \$('#year').on('click', function() {
        var url = '';
    
      var year_start = \$('input[name=\\'year_start\\']').val();
    
        if (year_start) {
            url += '&year_start=' + encodeURIComponent(year_start);
        }
      
      var year_end = \$('input[name=\\'year_end\\']').val();
    
        if (year_end) {
            url += '&year_end=' + encodeURIComponent(year_end);
        }
    
          location = 'index.php?route=extension/module/reports/info&user_token=";
        // line 416
        echo ($context["user_token"] ?? null);
        echo "' + url;
    });
    //--></script>
      <script type=\"text/javascript\"><!--
        \$('.datetime').datetimepicker({
            language: '";
        // line 421
        echo ($context["datepicker"] ?? null);
        echo "',
            pickDate: true
        });
    //--></script> 
    <script type=\"text/javascript\"><!--
        \$('#button-custom').on('click', function() {
            var url = '';
        
          var custom_date_start = \$('input[name=\\'start\\']').val();
        
            if (custom_date_start) {
                url += '&custom_date_start=' + encodeURIComponent(custom_date_start);
            }
          
          var custom_date_end = \$('input[name=\\'end\\']').val();
        
            if (custom_date_end) {
                url += '&custom_date_end=' + encodeURIComponent(custom_date_end);
            }
          
              location = 'index.php?route=extension/module/reports/info&user_token=";
        // line 441
        echo ($context["user_token"] ?? null);
        echo "' + url;
        });
        //--></script>
";
        // line 444
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "extension/module/reports.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  876 => 444,  870 => 441,  847 => 421,  839 => 416,  816 => 396,  793 => 376,  770 => 355,  762 => 350,  756 => 347,  750 => 343,  747 => 342,  739 => 337,  733 => 334,  728 => 331,  725 => 330,  717 => 325,  711 => 322,  706 => 319,  703 => 318,  695 => 313,  689 => 310,  684 => 307,  681 => 306,  671 => 301,  667 => 300,  661 => 297,  656 => 294,  653 => 293,  643 => 288,  639 => 287,  633 => 284,  628 => 281,  626 => 280,  619 => 276,  614 => 273,  604 => 268,  600 => 267,  594 => 264,  588 => 260,  585 => 259,  575 => 254,  571 => 253,  565 => 250,  559 => 246,  556 => 245,  546 => 240,  542 => 239,  536 => 236,  530 => 232,  527 => 231,  517 => 226,  513 => 225,  507 => 222,  501 => 218,  498 => 217,  488 => 212,  484 => 211,  478 => 208,  473 => 205,  470 => 204,  460 => 199,  456 => 198,  450 => 195,  445 => 192,  443 => 191,  436 => 187,  431 => 184,  421 => 179,  417 => 178,  411 => 175,  406 => 172,  403 => 171,  393 => 166,  389 => 165,  383 => 162,  378 => 159,  375 => 158,  365 => 153,  361 => 152,  355 => 149,  350 => 146,  347 => 145,  337 => 140,  333 => 139,  327 => 136,  322 => 133,  319 => 132,  311 => 127,  305 => 124,  300 => 121,  297 => 120,  287 => 115,  283 => 114,  277 => 111,  272 => 108,  269 => 107,  261 => 102,  255 => 99,  250 => 96,  247 => 95,  237 => 90,  233 => 89,  227 => 86,  222 => 83,  220 => 82,  213 => 78,  200 => 70,  194 => 67,  180 => 58,  174 => 55,  167 => 51,  161 => 48,  156 => 45,  148 => 43,  140 => 41,  137 => 40,  129 => 38,  121 => 36,  118 => 35,  110 => 33,  102 => 31,  100 => 30,  92 => 24,  84 => 19,  79 => 16,  77 => 15,  73 => 13,  62 => 11,  58 => 10,  53 => 8,  46 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "extension/module/reports.twig", "");
    }
}
