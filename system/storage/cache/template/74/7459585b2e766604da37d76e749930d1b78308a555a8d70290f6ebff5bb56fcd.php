<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/extension/module/sellers_list.twig */
class __TwigTemplate_a9cc5ec06207c8322457b88861d16c41bfbd3f27fb385a3c8643021a0e7679c5 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo "
<div id=\"seller\" class=\"container\">
  <ul class=\"breadcrumb\">
    ";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 5
            echo "    <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 5);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 5);
            echo "</a></li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 7
        echo "  </ul>
  <div class=\"container-fluid\">
    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <button type=\"button\" class=\"btn btn-primary pull-right\">Primary</button>

        <h3 class=\"panel-title\"><i class=\"fa fa-list\"></i> ";
        // line 13
        echo ($context["text_seller_list"] ?? null);
        echo "</h3>
      </div>
      <div class=\"panel-body\">
          <div class=\"table-responsive\">
            <table class=\"table table-bordered table-hover\">
              <thead>
                <tr style=\"color: #3366ff; font-weight: bold;\">
                  <td class=\"text-center\" style=\"width: 80px;\">Image</td>
                  <td class=\"text-left\">Seller Name</td>
                  <td class=\"text-center\" style=\"width: 120px;\">View Seller Detail</td>
                </tr>
              </thead>
              <tbody>
                ";
        // line 26
        if (($context["sellers"] ?? null)) {
            // line 27
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["sellers"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["seller"]) {
                // line 28
                echo "                <tr>
                  <td class=\"text-center\">";
                // line 29
                if (twig_get_attribute($this->env, $this->source, $context["seller"], "image", [], "any", false, false, false, 29)) {
                    echo " <img src=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["seller"], "image", [], "any", false, false, false, 29);
                    echo "\" alt=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["seller"], "name", [], "any", false, false, false, 29);
                    echo "\" class=\"img-thumbnail\" /> ";
                } else {
                    echo " <span class=\"img-thumbnail list\"><i class=\"fa fa-camera fa-2x\"></i></span> ";
                }
                echo "</td>
                  <td class=\"text-left\">";
                // line 30
                echo twig_get_attribute($this->env, $this->source, $context["seller"], "name", [], "any", false, false, false, 30);
                echo "</td>
                  <td class=\"text-center\"><a href=\"";
                // line 31
                echo twig_get_attribute($this->env, $this->source, $context["seller"], "href", [], "any", false, false, false, 31);
                echo "\" data-toggle=\"tooltip\" title=\"";
                echo ($context["button_view"] ?? null);
                echo "\" class=\"btn btn-primary\"><i class=\"fa fa-eye\"></i></a></td>
                </tr>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['seller'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 34
            echo "                ";
        } else {
            // line 35
            echo "                <tr>
                  <td class=\"text-center\" colspan=\"4\">";
            // line 36
            echo ($context["text_no_results"] ?? null);
            echo "</td>
                </tr>
                ";
        }
        // line 39
        echo "              </tbody>
            </table>
          </div>
        <div class=\"row\">
          <div class=\"col-sm-6 text-left\">";
        // line 43
        echo ($context["pagination"] ?? null);
        echo "</div>
          <div class=\"col-sm-6 text-right\">";
        // line 44
        echo ($context["results"] ?? null);
        echo "</div>
        </div>
      </div>
    </div>
  </div>
";
        // line 49
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "default/template/extension/module/sellers_list.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  149 => 49,  141 => 44,  137 => 43,  131 => 39,  125 => 36,  122 => 35,  119 => 34,  108 => 31,  104 => 30,  92 => 29,  89 => 28,  84 => 27,  82 => 26,  66 => 13,  58 => 7,  47 => 5,  43 => 4,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/extension/module/sellers_list.twig", "");
    }
}
