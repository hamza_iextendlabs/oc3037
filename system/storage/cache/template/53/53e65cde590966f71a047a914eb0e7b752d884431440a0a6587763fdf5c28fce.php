<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* extension/module/reports.twig */
class __TwigTemplate_e758b37abfd7695c45545866651d990644852c5a2b828660e86415c4dd7889d4 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
    <div class=\"page-header\">
        <div class=\"container-fluid\">
        <div class=\"pull-right\">
            <a href=\"";
        // line 6
        echo ($context["report_setting"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["text_setting"] ?? null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-cog fw\"></i></a>
        </div>
            <h1>";
        // line 8
        echo ($context["heading_title"] ?? null);
        echo "</h1>
            <ul class=\"breadcrumb\">
                ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 11
            echo "                <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 11);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 11);
            echo "</a></li>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "            </ul>
        </div>
        ";
        // line 15
        if (($context["isFirstLoad"] ?? null)) {
            // line 16
            echo "            <div class=\"col-md-12\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid#4cb64c; border-radius: 15px; margin-left: 10px;\">
                    <div class=\"text-success text-center mt-3 panel-body\">
                        <h4><b>";
            // line 19
            echo ($context["isFirstLoad"] ?? null);
            echo "</b></h4>
                    </div>
                </div>
            </div>
            ";
        }
        // line 24
        echo "    </div>
    <div class=\"jumbotron\">
        <div class=\"panel panel-default\">
            <div class=\"panel-heading\">
            <div class=\"pull-right\"><a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"><h2><i class=\"fa fa-calendar\"></i> <i class=\"caret\"></h2></i></a>
                <ul id=\"range\" class=\"dropdown-menu dropdown-menu-right\">
                    ";
        // line 30
        if ((($context["text_sales_report"] ?? null) == "Daily Sales Report")) {
            // line 31
            echo "                  <li id=\"day\" class=\"active\"><a>Today</a></li> <input type=\"hidden\" value=\"";
            echo ($context["day_start"] ?? null);
            echo "\" name=\"day_start\"> <input type=\"hidden\" value=\"";
            echo ($context["day_end"] ?? null);
            echo "\" name=\"day_end\">
                    ";
        } else {
            // line 33
            echo "                  <li id=\"day\"><a>Today</a></li> <input type=\"hidden\" value=\"";
            echo ($context["day_start"] ?? null);
            echo "\" name=\"day_start\"> <input type=\"hidden\" value=\"";
            echo ($context["day_end"] ?? null);
            echo "\" name=\"day_end\">
                    ";
        }
        // line 35
        echo "                    ";
        if ((($context["text_sales_report"] ?? null) == "Monthly Sales Report")) {
            // line 36
            echo "                  <li id=\"month\" class=\"active\"><a>Month</a></li> <input type=\"hidden\" value=\"";
            echo ($context["month_start"] ?? null);
            echo "\" name=\"month_start\"> <input type=\"hidden\" value=\"";
            echo ($context["month_end"] ?? null);
            echo "\" name=\"month_end\">
                    ";
        } else {
            // line 38
            echo "                  <li id=\"month\"><a>Month</a></li> <input type=\"hidden\" value=\"";
            echo ($context["month_start"] ?? null);
            echo "\" name=\"month_start\"> <input type=\"hidden\" value=\"";
            echo ($context["month_end"] ?? null);
            echo "\" name=\"month_end\">
                    ";
        }
        // line 40
        echo "                    ";
        if ((($context["text_sales_report"] ?? null) == "Yearly Sales Report")) {
            // line 41
            echo "                  <li id=\"year\" class=\"active\"><a>Year</a></li> <input type=\"hidden\" value=\"";
            echo ($context["year_start"] ?? null);
            echo "\" name=\"year_start\"> <input type=\"hidden\" value=\"";
            echo ($context["year_end"] ?? null);
            echo "\" name=\"year_end\">
                    ";
        } else {
            // line 43
            echo "                  <li id=\"year\"><a>Year</a></li> <input type=\"hidden\" value=\"";
            echo ($context["year_start"] ?? null);
            echo "\" name=\"year_start\"> <input type=\"hidden\" value=\"";
            echo ($context["year_end"] ?? null);
            echo "\" name=\"year_end\">
                    ";
        }
        // line 45
        echo "                  <li id=\"day\"><a>
                    <div class=\"pull-right\"><a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"><h2><i class=\"fa fa-calendar\"></i> <i class=\"caret\"></h2></i></a>
                    <ul id=\"range\" class=\"dropdown-menu dropdown-menu-right\"></a>
                        <li id=\"day\"><a>Today</a>
                    </ul>
                    </div>
                    </li>
                </ul>
              </div>
            <h2 class=\"panel-tital\">";
        // line 54
        echo ($context["text_sales_report"] ?? null);
        echo "</h2>
            </div>
        </div>
        <div class=\"row w-100 container-fluid\">
            ";
        // line 58
        if ((($context["order_status"] ?? null) == "on")) {
            // line 59
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid#f3a638; border-radius: 15px; height: 185px;\">
                    <div class=\"text-warning text-center mt-3 panel-body\">
                        <h4>";
            // line 62
            echo ($context["text_total_order"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-warning text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 65
            echo ($context["total_order"] ?? null);
            echo "</h1>
                        <a href=\"";
            // line 66
            echo ($context["href_order"] ?? null);
            echo "\"> <h5 class=\"text-warning\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 71
        echo "            ";
        if ((($context["sales_status"] ?? null) == "on")) {
            // line 72
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid #4cb657; border-radius: 15px; height: 185px;\">
                    <div class=\"text-success text-center mt-3 panel-body\">
                        <h4>";
            // line 75
            echo ($context["text_sales"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-success text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 78
            echo ($context["sales"] ?? null);
            echo "</h1>
                    </div>
                </div>
            </div>
            ";
        }
        // line 83
        echo "            ";
        if ((($context["return_status"] ?? null) == "on")) {
            // line 84
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid #e3503e; border-radius: 15px; height: 185px;\">
                    <div class=\"text-danger text-center mt-3 panel-body\">
                        <h4>";
            // line 87
            echo ($context["text_return"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-danger text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 90
            echo ($context["return"] ?? null);
            echo "</h1>
                        <a href=\"";
            // line 91
            echo ($context["href_refunded"] ?? null);
            echo "\"> <h5 class=\"text-danger\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 96
        echo "            ";
        if ((($context["tax_status"] ?? null) == "on")) {
            // line 97
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid #31708f; border-radius: 15px; height: 185px;\">
                    <div class=\"text-info text-center mt-3 panel-body\">
                        <h4>";
            // line 100
            echo ($context["text_tax"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-info text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 103
            echo ($context["tax"] ?? null);
            echo "</h1>
                    </div>
                </div>
            </div>
            ";
        }
        // line 108
        echo "            ";
        if ((($context["complete_order_status"] ?? null) == "on")) {
            // line 109
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid#f3a638; border-radius: 15px; height: 185px;\">
                    <div class=\"text-warning text-center mt-3 panel-body\">
                        <h4>";
            // line 112
            echo ($context["text_complete_order"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-warning text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 115
            echo ($context["total_complete_order"] ?? null);
            echo "</h1>
                        <a href=\"";
            // line 116
            echo ($context["href_complete"] ?? null);
            echo "\"> <h5 class=\"text-warning\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 121
        echo "            ";
        if ((($context["pending_order_status"] ?? null) == "on")) {
            // line 122
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid #4cb657; border-radius: 15px; height: 185px;\">
                    <div class=\"text-success text-center mt-3 panel-body\">
                        <h4>";
            // line 125
            echo ($context["text_pending_order"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-success text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 128
            echo ($context["total_panding_order"] ?? null);
            echo "</h1>
                        <a href=\"";
            // line 129
            echo ($context["href_panding"] ?? null);
            echo "\"> <h5 class=\"text-success\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 134
        echo "            ";
        if ((($context["canceled_order_status"] ?? null) == "on")) {
            // line 135
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid #e3503e; border-radius: 15px; height: 185px;\">
                    <div class=\"text-danger text-center mt-3 panel-body\">
                        <h4>";
            // line 138
            echo ($context["text_canceled_order"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-danger text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 141
            echo ($context["total_canceled_order"] ?? null);
            echo "</h1>
                        <a href=\"";
            // line 142
            echo ($context["href_canceled"] ?? null);
            echo "\"> <h5 class=\"text-danger\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 147
        echo "            ";
        if ((($context["refunded_order_status"] ?? null) == "on")) {
            // line 148
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid #31708f; border-radius: 15px; height: 185px;\">
                    <div class=\"text-info text-center mt-3 panel-body\">
                        <h4>";
            // line 151
            echo ($context["text_refunded_order"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-info text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 154
            echo ($context["total_refunded_order"] ?? null);
            echo "</h1>
                        <a href=\"";
            // line 155
            echo ($context["href_refunded"] ?? null);
            echo "\"> <h5 class=\"text-info\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 160
        echo "        </div>
        <div class=\"panel panel-default\">
            <div class=\"panel-heading\">
                <h2 class=\"panel-tital\">";
        // line 163
        echo ($context["text_product_report"] ?? null);
        echo "</h2>
            </div>
        </div>
        <div class=\"row w-100 container-fluid\">
            ";
        // line 167
        if ((($context["product_status"] ?? null) == "on")) {
            // line 168
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid#f3a638; border-radius: 15px; height: 185px;\">
                    <div class=\"text-warning text-center mt-3 panel-body\">
                        <h4>";
            // line 171
            echo ($context["text_total_product"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-warning text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 174
            echo ($context["total_product"] ?? null);
            echo "</h1>
                        <a href=\"";
            // line 175
            echo ($context["href_product"] ?? null);
            echo "\"> <h5 class=\"text-warning\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 180
        echo "            ";
        if ((($context["outofstock_product_status"] ?? null) == "on")) {
            // line 181
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid #4cb657; border-radius: 15px; height: 185px;\">
                    <div class=\"text-success text-center mt-3 panel-body\">
                        <h4>";
            // line 184
            echo ($context["text_total_outOfStock_product"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-success text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 187
            echo ($context["total_OutOfStock_product"] ?? null);
            echo "</h1>
                        <a href=\"";
            // line 188
            echo ($context["href_outofstock_product"] ?? null);
            echo "\"> <h5 class=\"text-success\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 193
        echo "            ";
        if ((($context["lowestselling_product_status"] ?? null) == "on")) {
            // line 194
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\"
                    style=\"border: 1px solid #e3503e; border-radius: 15px; height: 185px;\">
                    <div class=\"text-danger text-center mt-3 panel-body\">
                        <h4>";
            // line 198
            echo ($context["text_lowest_selling_product"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-danger text-center mt-2 panel-body\">
                        <h2><b>";
            // line 201
            echo ($context["total_LowestSelling_product"] ?? null);
            echo "</b></h2>
                        <a href=\"";
            // line 202
            echo ($context["href_lowestselling_product"] ?? null);
            echo "\"> <h5 class=\"text-danger\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 207
        echo "            ";
        if ((($context["highestselling_product_status"] ?? null) == "on")) {
            // line 208
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\"
                    style=\"border: 1px solid #31708f; border-radius: 15px; height: 185px;\">
                    <div class=\"text-info text-center mt-3 panel-body\">
                        <h4>";
            // line 212
            echo ($context["text_highest_selling_product"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-info text-center mt-2 panel-body\">
                        <h2><b>";
            // line 215
            echo ($context["total_HighestSelling_product"] ?? null);
            echo "</b></h2>
                        <a href=\"";
            // line 216
            echo ($context["href_highestselling_product"] ?? null);
            echo "\"> <h5 class=\"text-info\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 221
        echo "            ";
        if ((($context["topview_product_status"] ?? null) == "on")) {
            // line 222
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\"
                    style=\"border: 1px solid#f3a638; border-radius: 15px; height: 185px;\">
                    <div class=\"text-warning text-center mt-3 panel-body\">
                        <h4>";
            // line 226
            echo ($context["text_TopView_product"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-warning text-center mt-2 panel-body\">
                        <h2><b>";
            // line 229
            echo ($context["total_TopView_product"] ?? null);
            echo "</b></h2>
                        <a href=\"";
            // line 230
            echo ($context["href_topView_product"] ?? null);
            echo "\"> <h5 class=\"text-warning\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 235
        echo "            ";
        if ((($context["lowestview_product_status"] ?? null) == "on")) {
            // line 236
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\"
                    style=\"border: 1px solid #4cb657; border-radius: 15px; height: 185px;\">
                    <div class=\"text-success text-center mt-3 panel-body\">
                        <h4>";
            // line 240
            echo ($context["text_LowestView_product"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-success text-center mt-2 panel-body\">
                        <h2><b>";
            // line 243
            echo ($context["total_LowestView_product"] ?? null);
            echo "</b></h2>
                        <a href=\"";
            // line 244
            echo ($context["href_lowestview_product"] ?? null);
            echo "\"> <h5 class=\"text-success\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 249
        echo "        </div>
        <div class=\"panel panel-default\">
            <div class=\"panel-heading\">
                <h2 class=\"panel-tital\">";
        // line 252
        echo ($context["text_category_report"] ?? null);
        echo "</h2>
            </div>
        </div>
        <div class=\"row w-100 container-fluid\">
            ";
        // line 256
        if ((($context["customers_status"] ?? null) == "on")) {
            // line 257
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid#f3a638; border-radius: 15px; height: 185px;\">
                    <div class=\"text-warning text-center mt-3 panel-body\">
                        <h4>";
            // line 260
            echo ($context["text_total_customer"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-warning text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 263
            echo ($context["total_customer"] ?? null);
            echo "</h1>
                        <a href=\"";
            // line 264
            echo ($context["href_customer"] ?? null);
            echo "\"> <h5 class=\"text-warning\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 269
        echo "            ";
        if ((($context["category_status"] ?? null) == "on")) {
            // line 270
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid #4cb657; border-radius: 15px; height: 185px;\">
                    <div class=\"text-success text-center mt-3 panel-body\">
                        <h4>";
            // line 273
            echo ($context["text_total_category"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-success text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 276
            echo ($context["total_category"] ?? null);
            echo "</h1>
                        <a href=\"";
            // line 277
            echo ($context["href_category"] ?? null);
            echo "\"> <h5 class=\"text-success\">";
            echo ($context["text_view"] ?? null);
            echo "</h5> </a>
                    </div>
                </div>
            </div>
            ";
        }
        // line 282
        echo "            ";
        if ((($context["enable_category_status"] ?? null) == "on")) {
            // line 283
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid #e3503e; border-radius: 15px; height: 185px;\">
                    <div class=\"text-danger text-center mt-3 panel-body\">
                        <h4>";
            // line 286
            echo ($context["text_total_enable_category"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-danger text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 289
            echo ($context["total_enable_category"] ?? null);
            echo "</h1>
                    </div>
                </div>
            </div>
            ";
        }
        // line 294
        echo "            ";
        if ((($context["disable_category_status"] ?? null) == "on")) {
            // line 295
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid #31708f; border-radius: 15px; height: 185px;\">
                    <div class=\"text-info text-center mt-3 panel-body\">
                        <h4>";
            // line 298
            echo ($context["text_total_disable_category"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-info text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 301
            echo ($context["total_disable_category"] ?? null);
            echo "</h1>
                    </div>
                </div>
            </div>
            ";
        }
        // line 306
        echo "            ";
        if ((($context["nanufacturer_status"] ?? null) == "on")) {
            // line 307
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\" style=\"border: 1px solid#f3a638; border-radius: 15px; height: 185px;\">
                    <div class=\"text-warning text-center mt-3 panel-body\">
                        <h4>";
            // line 310
            echo ($context["text_total_manufacturer"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-warning text-center mt-2 panel-body\">
                        <h1 style=\"font-size:40px\">";
            // line 313
            echo ($context["total_manufacturer"] ?? null);
            echo "</h1>
                    </div>
                </div>
            </div>
            ";
        }
        // line 318
        echo "            ";
        if ((($context["topbrand_status"] ?? null) == "on")) {
            // line 319
            echo "            <div class=\"col-md-3\">
                <div class=\"panel panel-default mx-sm-1 p-3\"
                    style=\"border: 1px solid #4cb657; border-radius: 15px; height: 185px;\">
                    <div class=\"text-success text-center mt-3 panel-body\">
                        <h4>";
            // line 323
            echo ($context["text_top_brand"] ?? null);
            echo "</h4>
                    </div>
                    <div class=\"text-success text-center mt-2 panel-body\">
                        <h2><b>";
            // line 326
            echo ($context["top_brand"] ?? null);
            echo "</b></h2>
                    </div>
                </div>
            </div>
            ";
        }
        // line 331
        echo "        </div>
    </div>
</div>
</div>
<script type=\"text/javascript\"><!--
    \$('#day').on('click', function() {
        
        var url = '';
    
      var day_start = \$('input[name=\\'day_start\\']').val();
    
        if (day_start) {
            url += '&day_start=' + (day_start);
        }
      
      var day_end = \$('input[name=\\'day_end\\']').val();
    
        if (day_end) {
            url += '&day_end=' + (day_end);
        }
    
          location = 'index.php?route=extension/module/reports/info&user_token=";
        // line 352
        echo ($context["user_token"] ?? null);
        echo "' + url;
    });
    //--></script>

<script type=\"text/javascript\"><!--
    \$('#month').on('click', function() {
        var url = '';
    
      var month_start = \$('input[name=\\'month_start\\']').val();
    
        if (month_start) {
            url += '&month_start=' + encodeURIComponent(month_start);
        }
      
      var month_end = \$('input[name=\\'month_end\\']').val();
    
        if (month_end) {
            url += '&month_end=' + encodeURIComponent(month_end);
        }
    
          location = 'index.php?route=extension/module/reports/info&user_token=";
        // line 372
        echo ($context["user_token"] ?? null);
        echo "' + url;
    });
    //--></script>

<script type=\"text/javascript\"><!--
    \$('#year').on('click', function() {
        var url = '';
    
      var year_start = \$('input[name=\\'year_start\\']').val();
    
        if (year_start) {
            url += '&year_start=' + encodeURIComponent(year_start);
        }
      
      var year_end = \$('input[name=\\'year_end\\']').val();
    
        if (year_end) {
            url += '&year_end=' + encodeURIComponent(year_end);
        }
    
          location = 'index.php?route=extension/module/reports/info&user_token=";
        // line 392
        echo ($context["user_token"] ?? null);
        echo "' + url;
    });
    //--></script>
";
        // line 395
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "extension/module/reports.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  799 => 395,  793 => 392,  770 => 372,  747 => 352,  724 => 331,  716 => 326,  710 => 323,  704 => 319,  701 => 318,  693 => 313,  687 => 310,  682 => 307,  679 => 306,  671 => 301,  665 => 298,  660 => 295,  657 => 294,  649 => 289,  643 => 286,  638 => 283,  635 => 282,  625 => 277,  621 => 276,  615 => 273,  610 => 270,  607 => 269,  597 => 264,  593 => 263,  587 => 260,  582 => 257,  580 => 256,  573 => 252,  568 => 249,  558 => 244,  554 => 243,  548 => 240,  542 => 236,  539 => 235,  529 => 230,  525 => 229,  519 => 226,  513 => 222,  510 => 221,  500 => 216,  496 => 215,  490 => 212,  484 => 208,  481 => 207,  471 => 202,  467 => 201,  461 => 198,  455 => 194,  452 => 193,  442 => 188,  438 => 187,  432 => 184,  427 => 181,  424 => 180,  414 => 175,  410 => 174,  404 => 171,  399 => 168,  397 => 167,  390 => 163,  385 => 160,  375 => 155,  371 => 154,  365 => 151,  360 => 148,  357 => 147,  347 => 142,  343 => 141,  337 => 138,  332 => 135,  329 => 134,  319 => 129,  315 => 128,  309 => 125,  304 => 122,  301 => 121,  291 => 116,  287 => 115,  281 => 112,  276 => 109,  273 => 108,  265 => 103,  259 => 100,  254 => 97,  251 => 96,  241 => 91,  237 => 90,  231 => 87,  226 => 84,  223 => 83,  215 => 78,  209 => 75,  204 => 72,  201 => 71,  191 => 66,  187 => 65,  181 => 62,  176 => 59,  174 => 58,  167 => 54,  156 => 45,  148 => 43,  140 => 41,  137 => 40,  129 => 38,  121 => 36,  118 => 35,  110 => 33,  102 => 31,  100 => 30,  92 => 24,  84 => 19,  79 => 16,  77 => 15,  73 => 13,  62 => 11,  58 => 10,  53 => 8,  46 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "extension/module/reports.twig", "");
    }
}
