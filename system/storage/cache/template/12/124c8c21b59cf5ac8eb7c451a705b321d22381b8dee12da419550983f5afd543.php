<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/extension/module/seller_form.twig */
class __TwigTemplate_2df68aca690974e20d9fbd40f9b57ea7915e56ea690100dcc12d83751304e267 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container\">
      <div>
      <h1>";
        // line 6
        echo ($context["text_seller_form"] ?? null);
        echo "</h1>
      <ul class=\"breadcrumb\">
        ";
        // line 8
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 9
            echo "        <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 9);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 9);
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 11
        echo "      </ul>
    </div>
  </div>
  <div class=\"container\">";
        // line 14
        if (($context["error_warning"] ?? null)) {
            // line 15
            echo "    <div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo ($context["error_warning"] ?? null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 19
        echo "    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\"><i class=\"fa fa-pencil\"></i> ";
        // line 21
        echo ($context["text_form"] ?? null);
        echo "</h3>
      </div>
      <div class=\"panel-body\">
        <form action=\"";
        // line 24
        echo ($context["action"] ?? null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-seller\" class=\"form-horizontal\">
          <div class=\"tab-content\">
            <div class=\"form-group required\">
              <label class=\"col-sm-2 control-label\" for=\"input-seller-name\">";
        // line 27
        echo ($context["entry_seller_name"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\">
                <input type=\"text\" name=\"seller_name\" value=\"";
        // line 29
        echo ($context["seller_name"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_seller_name"] ?? null);
        echo "\" id=\"input-seller-name\" class=\"form-control\" />
                ";
        // line 30
        if (($context["error_seller_name"] ?? null)) {
            // line 31
            echo "                <div class=\"text-danger\">";
            echo ($context["error_seller_name"] ?? null);
            echo "</div>
                ";
        }
        // line 32
        echo " </div>
            </div>
            <div class=\"form-group required\">
              <label class=\"col-sm-2 control-label\" for=\"input-seller-description\">";
        // line 35
        echo ($context["entry_seller_description"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\">
                <textarea name=\"seller_description\" placeholder=\"";
        // line 37
        echo ($context["entry_seller_description"] ?? null);
        echo "\" id=\"input-seller-description\" data-toggle=\"summernote\" data-lang=\"";
        echo ($context["summernote"] ?? null);
        echo "\" class=\"form-control\" style=\"height: 150px;\">";
        echo ($context["seller_description"] ?? null);
        echo "</textarea>
                ";
        // line 38
        if (($context["error_seller_description"] ?? null)) {
            // line 39
            echo "                <div class=\"text-danger\">";
            echo ($context["error_seller_description"] ?? null);
            echo "</div>
                ";
        }
        // line 40
        echo " </div>
            </div>
            <div class=\"form-group required\">
              <label class=\"col-sm-2 control-label\" for=\"input-email\">";
        // line 43
        echo ($context["entry_email"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\">
                <input type=\"text\" name=\"email\" value=\"";
        // line 45
        echo ($context["email"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_email"] ?? null);
        echo "\" id=\"input-email\" class=\"form-control\" />
                ";
        // line 46
        if (($context["error_email"] ?? null)) {
            // line 47
            echo "                <div class=\"text-danger\">";
            echo ($context["error_email"] ?? null);
            echo "</div>
                ";
        }
        // line 48
        echo "</div>
            </div>
            <div class=\"form-group required\">
              <label class=\"col-sm-2 control-label\" for=\"input-telephone\">";
        // line 51
        echo ($context["entry_telephone"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\">
                <input type=\"text\" name=\"telephone\" value=\"";
        // line 53
        echo ($context["telephone"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_telephone"] ?? null);
        echo "\" id=\"input-telephone\" class=\"form-control\" />
                ";
        // line 54
        if (($context["error_telephone"] ?? null)) {
            // line 55
            echo "                <div class=\"text-danger\">";
            echo ($context["error_telephone"] ?? null);
            echo "</div>
                ";
        }
        // line 56
        echo "</div>
            </div><div class=\"form-group required\">
              <label class=\"col-sm-2 control-label\" for=\"input-address\">";
        // line 58
        echo ($context["entry_address"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\">
                <input type=\"text\" name=\"address\" value=\"";
        // line 60
        echo ($context["address"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_address"] ?? null);
        echo "\" id=\"input-address\" class=\"form-control\" />
                ";
        // line 61
        if (($context["error_address"] ?? null)) {
            // line 62
            echo "                <div class=\"text-danger\">";
            echo ($context["error_address"] ?? null);
            echo "</div>
                ";
        }
        // line 63
        echo "</div>
            </div><div class=\"form-group required\">
              <label class=\"col-sm-2 control-label\" for=\"input-store-name\">";
        // line 65
        echo ($context["entry_store_name"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\">
                <input type=\"text\" name=\"store_name\" value=\"";
        // line 67
        echo ($context["store_name"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_store_name"] ?? null);
        echo "\" id=\"input-store-name\" class=\"form-control\" />
                ";
        // line 68
        if (($context["error_store_name"] ?? null)) {
            // line 69
            echo "                <div class=\"text-danger\">";
            echo ($context["error_store_name"] ?? null);
            echo "</div>
                ";
        }
        // line 70
        echo "</div>
            </div>
            <div class=\"form-group\">
              <label class=\"col-sm-2 control-label\" for=\"input-logo\">";
        // line 73
        echo ($context["entry_logo"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\">
                <input class=\"btn btn-sm\" type=\"file\" name=\"logo\" class=\"form-control\">
                ";
        // line 76
        if (($context["error_logo"] ?? null)) {
            // line 77
            echo "                <div class=\"text-danger\">";
            echo ($context["error_logo"] ?? null);
            echo "</div>
                ";
        }
        // line 79
        echo "                </div>
            </div>
            <div class=\"form-group\">
              <label class=\"col-sm-2 control-label\" for=\"input-banner\">";
        // line 82
        echo ($context["entry_banner_image"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\">
                <input class=\"btn btn-sm\" type=\"file\" name=\"banner\" class=\"form-control\">
                ";
        // line 85
        if (($context["error_banner"] ?? null)) {
            // line 86
            echo "                <div class=\"text-danger\">";
            echo ($context["error_banner"] ?? null);
            echo "</div>
                ";
        }
        // line 88
        echo "                </div>
            </div>
          </div>
        </form>
      </div>
    </div>
    <div class=\"pull-right\">
      <button type=\"submit\" form=\"form-seller\" data-toggle=\"tooltip\" title=\"";
        // line 95
        echo ($context["button_save"] ?? null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></button>
      <a href=\"";
        // line 96
        echo ($context["cancel"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_cancel"] ?? null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
    </div>
  </div>
  <link href=\"view/javascript/codemirror/lib/codemirror.css\" rel=\"stylesheet\" />
  <link href=\"view/javascript/codemirror/theme/monokai.css\" rel=\"stylesheet\" />
  <script type=\"text/javascript\" src=\"view/javascript/codemirror/lib/codemirror.js\"></script> 
  <script type=\"text/javascript\" src=\"view/javascript/codemirror/lib/xml.js\"></script> 
  <script type=\"text/javascript\" src=\"view/javascript/codemirror/lib/formatting.js\"></script> 
  <script type=\"text/javascript\" src=\"view/javascript/summernote/summernote.js\"></script>
  <link href=\"view/javascript/summernote/summernote.css\" rel=\"stylesheet\" />
  <script type=\"text/javascript\" src=\"view/javascript/summernote/summernote-image-attributes.js\"></script> 
  <script type=\"text/javascript\" src=\"view/javascript/summernote/opencart.js\"></script> 
  <script type=\"text/javascript\"><!--
\$('#language a:first').tab('show');
//--></script></div>
";
        // line 111
        echo ($context["footer"] ?? null);
        echo " 
";
    }

    public function getTemplateName()
    {
        return "default/template/extension/module/seller_form.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  307 => 111,  287 => 96,  283 => 95,  274 => 88,  268 => 86,  266 => 85,  260 => 82,  255 => 79,  249 => 77,  247 => 76,  241 => 73,  236 => 70,  230 => 69,  228 => 68,  222 => 67,  217 => 65,  213 => 63,  207 => 62,  205 => 61,  199 => 60,  194 => 58,  190 => 56,  184 => 55,  182 => 54,  176 => 53,  171 => 51,  166 => 48,  160 => 47,  158 => 46,  152 => 45,  147 => 43,  142 => 40,  136 => 39,  134 => 38,  126 => 37,  121 => 35,  116 => 32,  110 => 31,  108 => 30,  102 => 29,  97 => 27,  91 => 24,  85 => 21,  81 => 19,  73 => 15,  71 => 14,  66 => 11,  55 => 9,  51 => 8,  46 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/extension/module/seller_form.twig", "");
    }
}
