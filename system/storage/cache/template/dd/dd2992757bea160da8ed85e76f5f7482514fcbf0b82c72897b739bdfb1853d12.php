<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* extension/module/report_setting.twig */
class __TwigTemplate_5abc000f00a273d27cc0443256d8fd9fa1d24b4cdcd72bc8447bcc2d6c648d45 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
        <button type=\"submit\" form=\"form-module\" data-toggle=\"tooltip\" title=\"";
        // line 6
        echo ($context["button_save"] ?? null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></button>
        <a href=\"";
        // line 7
        echo ($context["cancel"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_cancel"] ?? null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
      <h1>";
        // line 8
        echo ($context["heading_title"] ?? null);
        echo "</h1>
      <ul class=\"breadcrumb\">
        ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 11
            echo "        <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 11);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 11);
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "      </ul>
    </div>
  </div>
  <div class=\"container-fluid\">
    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\"><i class=\"fa fa-pencil\"></i> ";
        // line 19
        echo ($context["text_edit"] ?? null);
        echo "</h3>
      </div>
      <div class=\"panel-body\">
        <form action=\"";
        // line 22
        echo ($context["action"] ?? null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-module\" class=\"form-horizontal\">
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\">";
        // line 24
        echo ($context["entry_sales"] ?? null);
        echo "</label>
            <div class=\"col-sm-10\">
              <div class=\"well well-sm\" style=\"height: 150px; overflow: auto;\">
                <div class=\"checkbox\">
                  <label>
                  ";
        // line 29
        if (($context["module_order_status"] ?? null)) {
            // line 30
            echo "                    <input type=\"checkbox\" name=\"module_order_status\"  checked=\"checked\" />
                     ";
            // line 31
            echo ($context["text_order"] ?? null);
            echo "
                  ";
        } else {
            // line 33
            echo "                    <input type=\"checkbox\" name=\"module_order_status\"  />
                     ";
            // line 34
            echo ($context["text_order"] ?? null);
            echo "
                     ";
        }
        // line 36
        echo "                      <br>
                     ";
        // line 37
        if (($context["module_sales_status"] ?? null)) {
            // line 38
            echo "                    <input type=\"checkbox\" name=\"module_sales_status\"  checked=\"checked\" />
                     ";
            // line 39
            echo ($context["text_sales"] ?? null);
            echo " 
                  ";
        } else {
            // line 41
            echo "                    <input type=\"checkbox\" name=\"module_sales_status\"  />
                     ";
            // line 42
            echo ($context["text_sales"] ?? null);
            echo " 
                     ";
        }
        // line 44
        echo "                      <br>
                     ";
        // line 45
        if (($context["module_return_status"] ?? null)) {
            // line 46
            echo "                    <input type=\"checkbox\" name=\"module_return_status\"  checked=\"checked\" />
                     ";
            // line 47
            echo ($context["text_return"] ?? null);
            echo " 
                  ";
        } else {
            // line 49
            echo "                    <input type=\"checkbox\" name=\"module_return_status\"  />
                     ";
            // line 50
            echo ($context["text_return"] ?? null);
            echo " 
                     ";
        }
        // line 52
        echo "                      <br>
                      ";
        // line 53
        if (($context["module_tax_status"] ?? null)) {
            // line 54
            echo "                    <input type=\"checkbox\" name=\"module_tax_status\"  checked=\"checked\" />
                     ";
            // line 55
            echo ($context["text_tax"] ?? null);
            echo " 
                  ";
        } else {
            // line 57
            echo "                    <input type=\"checkbox\" name=\"module_tax_status\"  />
                     ";
            // line 58
            echo ($context["text_tax"] ?? null);
            echo " 
                     ";
        }
        // line 60
        echo "                      <br>
                      ";
        // line 61
        if (($context["module_complete_order_status"] ?? null)) {
            // line 62
            echo "                    <input type=\"checkbox\" name=\"module_complete_order_status\"  checked=\"checked\" />
                     ";
            // line 63
            echo ($context["text_complete_order"] ?? null);
            echo " 
                  ";
        } else {
            // line 65
            echo "                    <input type=\"checkbox\" name=\"module_complete_order_status\"  />
                     ";
            // line 66
            echo ($context["text_complete_order"] ?? null);
            echo " 
                     ";
        }
        // line 68
        echo "                      <br>
                      ";
        // line 69
        if (($context["module_pending_order_status"] ?? null)) {
            // line 70
            echo "                    <input type=\"checkbox\" name=\"module_pending_order_status\"  checked=\"checked\" />
                     ";
            // line 71
            echo ($context["text_pending_order"] ?? null);
            echo " 
                  ";
        } else {
            // line 73
            echo "                    <input type=\"checkbox\" name=\"module_pending_order_status\"  />
                     ";
            // line 74
            echo ($context["text_pending_order"] ?? null);
            echo " 
                     ";
        }
        // line 76
        echo "                      <br>
                      ";
        // line 77
        if (($context["module_canceled_order_status"] ?? null)) {
            // line 78
            echo "                    <input type=\"checkbox\" name=\"module_canceled_order_status\"  checked=\"checked\" />
                     ";
            // line 79
            echo ($context["text_canceled_order"] ?? null);
            echo " 
                  ";
        } else {
            // line 81
            echo "                    <input type=\"checkbox\" name=\"module_canceled_order_status\"  />
                     ";
            // line 82
            echo ($context["text_canceled_order"] ?? null);
            echo " 
                     ";
        }
        // line 84
        echo "                      <br>
                      ";
        // line 85
        if (($context["module_refunded_order_status"] ?? null)) {
            // line 86
            echo "                    <input type=\"checkbox\" name=\"module_refunded_order_status\"  checked=\"checked\" />
                     ";
            // line 87
            echo ($context["text_refunded_order"] ?? null);
            echo " 
                  ";
        } else {
            // line 89
            echo "                    <input type=\"checkbox\" name=\"module_refunded_order_status\"  />
                     ";
            // line 90
            echo ($context["text_refunded_order"] ?? null);
            echo " 
                     ";
        }
        // line 92
        echo "                  </label>
                </div>
              </div>
              <button type=\"button\" onclick=\"\$(this).parent().find(':checkbox').prop('checked', true);\" class=\"btn btn-link\">";
        // line 95
        echo ($context["text_select_all"] ?? null);
        echo "</button> / <button type=\"button\" onclick=\"\$(this).parent().find(':checkbox').prop('checked', false);\" class=\"btn btn-link\">";
        echo ($context["text_unselect_all"] ?? null);
        echo "</button></div>
          </div>
          
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\">";
        // line 99
        echo ($context["entry_product"] ?? null);
        echo "</label>
            <div class=\"col-sm-10\">
              <div class=\"well well-sm\" style=\"height: 150px; overflow: auto;\">
                <div class=\"checkbox\">
                  <label>
                      ";
        // line 104
        if (($context["module_product_status"] ?? null)) {
            // line 105
            echo "                    <input type=\"checkbox\" name=\"module_product_status\"  checked=\"checked\" />
                     ";
            // line 106
            echo ($context["text_product"] ?? null);
            echo " 
                  ";
        } else {
            // line 108
            echo "                    <input type=\"checkbox\" name=\"module_product_status\"  />
                     ";
            // line 109
            echo ($context["text_product"] ?? null);
            echo " 
                     ";
        }
        // line 111
        echo "                      <br>
                      ";
        // line 112
        if (($context["module_outofstock_product_status"] ?? null)) {
            // line 113
            echo "                    <input type=\"checkbox\" name=\"module_outofstock_product_status\"  checked=\"checked\" />
                     ";
            // line 114
            echo ($context["text_outOfStock_product"] ?? null);
            echo " 
                  ";
        } else {
            // line 116
            echo "                    <input type=\"checkbox\" name=\"module_outofstock_product_status\"  />
                     ";
            // line 117
            echo ($context["text_outOfStock_product"] ?? null);
            echo " 
                     ";
        }
        // line 119
        echo "                      <br>
                      ";
        // line 120
        if (($context["module_lowestselling_product_status"] ?? null)) {
            // line 121
            echo "                    <input type=\"checkbox\" name=\"module_lowestselling_product_status\"  checked=\"checked\" />
                     ";
            // line 122
            echo ($context["text_lowest_selling_product"] ?? null);
            echo " 
                  ";
        } else {
            // line 124
            echo "                    <input type=\"checkbox\" name=\"module_lowestselling_product_status\"  />
                     ";
            // line 125
            echo ($context["text_lowest_selling_product"] ?? null);
            echo " 
                     ";
        }
        // line 127
        echo "                      <br>
                      ";
        // line 128
        if (($context["module_highestselling_product_status"] ?? null)) {
            // line 129
            echo "                    <input type=\"checkbox\" name=\"module_highestselling_product_status\"  checked=\"checked\" />
                     ";
            // line 130
            echo ($context["text_highest_selling_product"] ?? null);
            echo " 
                  ";
        } else {
            // line 132
            echo "                    <input type=\"checkbox\" name=\"module_highestselling_product_status\"  />
                     ";
            // line 133
            echo ($context["text_highest_selling_product"] ?? null);
            echo " 
                     ";
        }
        // line 135
        echo "                      <br>
                      ";
        // line 136
        if (($context["module_topview_product_status"] ?? null)) {
            // line 137
            echo "                    <input type=\"checkbox\" name=\"module_topview_product_status\"  checked=\"checked\" />
                     ";
            // line 138
            echo ($context["text_TopView_product"] ?? null);
            echo " 
                  ";
        } else {
            // line 140
            echo "                    <input type=\"checkbox\" name=\"module_topview_product_status\"  />
                     ";
            // line 141
            echo ($context["text_TopView_product"] ?? null);
            echo " 
                     ";
        }
        // line 143
        echo "                      <br>
                      ";
        // line 144
        if (($context["module_lowestview_product_status"] ?? null)) {
            // line 145
            echo "                    <input type=\"checkbox\" name=\"module_lowestview_product_status\"  checked=\"checked\" />
                     ";
            // line 146
            echo ($context["text_LowestView_product"] ?? null);
            echo " 
                  ";
        } else {
            // line 148
            echo "                    <input type=\"checkbox\" name=\"module_lowestview_product_status\"  />
                     ";
            // line 149
            echo ($context["text_LowestView_product"] ?? null);
            echo " 
                     ";
        }
        // line 151
        echo "                  </label>
                </div>
              </div>
              <button type=\"button\" onclick=\"\$(this).parent().find(':checkbox').prop('checked', true);\" class=\"btn btn-link\">";
        // line 154
        echo ($context["text_select_all"] ?? null);
        echo "</button> / <button type=\"button\" onclick=\"\$(this).parent().find(':checkbox').prop('checked', false);\" class=\"btn btn-link\">";
        echo ($context["text_unselect_all"] ?? null);
        echo "</button></div>
          </div>

          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\">";
        // line 158
        echo ($context["entry_category"] ?? null);
        echo "</label>
            <div class=\"col-sm-10\">
              <div class=\"well well-sm\" style=\"height: 150px; overflow: auto;\">
                <div class=\"checkbox\">
                  <label>
                      ";
        // line 163
        if (($context["module_customers_status"] ?? null)) {
            // line 164
            echo "                    <input type=\"checkbox\" name=\"module_customers_status\"  checked=\"checked\" />
                     ";
            // line 165
            echo ($context["text_customer"] ?? null);
            echo " 
                  ";
        } else {
            // line 167
            echo "                    <input type=\"checkbox\" name=\"module_customers_status\"  />
                     ";
            // line 168
            echo ($context["text_customer"] ?? null);
            echo " 
                     ";
        }
        // line 170
        echo "                      <br>
                      ";
        // line 171
        if (($context["module_category_status"] ?? null)) {
            // line 172
            echo "                    <input type=\"checkbox\" name=\"module_category_status\"  checked=\"checked\" />
                     ";
            // line 173
            echo ($context["text_category"] ?? null);
            echo " 
                  ";
        } else {
            // line 175
            echo "                    <input type=\"checkbox\" name=\"module_category_status\"  />
                     ";
            // line 176
            echo ($context["text_category"] ?? null);
            echo " 
                     ";
        }
        // line 178
        echo "                      <br>
                      ";
        // line 179
        if (($context["module_enable_category_status"] ?? null)) {
            // line 180
            echo "                    <input type=\"checkbox\" name=\"module_enable_category_status\"  checked=\"checked\" />
                     ";
            // line 181
            echo ($context["text_enable_category"] ?? null);
            echo " 
                  ";
        } else {
            // line 183
            echo "                    <input type=\"checkbox\" name=\"module_enable_category_status\"  />
                     ";
            // line 184
            echo ($context["text_enable_category"] ?? null);
            echo " 
                     ";
        }
        // line 186
        echo "                      <br>
                      ";
        // line 187
        if (($context["module_disable_category_status"] ?? null)) {
            // line 188
            echo "                    <input type=\"checkbox\" name=\"module_disable_category_status\"  checked=\"checked\" />
                     ";
            // line 189
            echo ($context["text_disable_category"] ?? null);
            echo " 
                  ";
        } else {
            // line 191
            echo "                    <input type=\"checkbox\" name=\"module_disable_category_status\"  />
                     ";
            // line 192
            echo ($context["text_disable_category"] ?? null);
            echo " 
                     ";
        }
        // line 194
        echo "                      <br>
                      ";
        // line 195
        if (($context["module_nanufacturer_status"] ?? null)) {
            // line 196
            echo "                    <input type=\"checkbox\" name=\"module_nanufacturer_status\"  checked=\"checked\" />
                     ";
            // line 197
            echo ($context["text_manufacturer"] ?? null);
            echo " 
                  ";
        } else {
            // line 199
            echo "                    <input type=\"checkbox\" name=\"module_nanufacturer_status\"  />
                     ";
            // line 200
            echo ($context["text_manufacturer"] ?? null);
            echo " 
                     ";
        }
        // line 202
        echo "                      <br>
                      ";
        // line 203
        if (($context["module_topbrand_status"] ?? null)) {
            // line 204
            echo "                    <input type=\"checkbox\" name=\"module_topbrand_status\"  checked=\"checked\" />
                     ";
            // line 205
            echo ($context["text_top_brand"] ?? null);
            echo " 
                  ";
        } else {
            // line 207
            echo "                    <input type=\"checkbox\" name=\"module_topbrand_status\"  />
                     ";
            // line 208
            echo ($context["text_top_brand"] ?? null);
            echo " 
                     ";
        }
        // line 210
        echo "                      <br>
                  </label>
                </div>
              </div>
              <button type=\"button\" onclick=\"\$(this).parent().find(':checkbox').prop('checked', true);\" class=\"btn btn-link\">";
        // line 214
        echo ($context["text_select_all"] ?? null);
        echo "</button> / <button type=\"button\" onclick=\"\$(this).parent().find(':checkbox').prop('checked', false);\" class=\"btn btn-link\">";
        echo ($context["text_unselect_all"] ?? null);
        echo "</button></div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
";
        // line 221
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "extension/module/report_setting.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  576 => 221,  564 => 214,  558 => 210,  553 => 208,  550 => 207,  545 => 205,  542 => 204,  540 => 203,  537 => 202,  532 => 200,  529 => 199,  524 => 197,  521 => 196,  519 => 195,  516 => 194,  511 => 192,  508 => 191,  503 => 189,  500 => 188,  498 => 187,  495 => 186,  490 => 184,  487 => 183,  482 => 181,  479 => 180,  477 => 179,  474 => 178,  469 => 176,  466 => 175,  461 => 173,  458 => 172,  456 => 171,  453 => 170,  448 => 168,  445 => 167,  440 => 165,  437 => 164,  435 => 163,  427 => 158,  418 => 154,  413 => 151,  408 => 149,  405 => 148,  400 => 146,  397 => 145,  395 => 144,  392 => 143,  387 => 141,  384 => 140,  379 => 138,  376 => 137,  374 => 136,  371 => 135,  366 => 133,  363 => 132,  358 => 130,  355 => 129,  353 => 128,  350 => 127,  345 => 125,  342 => 124,  337 => 122,  334 => 121,  332 => 120,  329 => 119,  324 => 117,  321 => 116,  316 => 114,  313 => 113,  311 => 112,  308 => 111,  303 => 109,  300 => 108,  295 => 106,  292 => 105,  290 => 104,  282 => 99,  273 => 95,  268 => 92,  263 => 90,  260 => 89,  255 => 87,  252 => 86,  250 => 85,  247 => 84,  242 => 82,  239 => 81,  234 => 79,  231 => 78,  229 => 77,  226 => 76,  221 => 74,  218 => 73,  213 => 71,  210 => 70,  208 => 69,  205 => 68,  200 => 66,  197 => 65,  192 => 63,  189 => 62,  187 => 61,  184 => 60,  179 => 58,  176 => 57,  171 => 55,  168 => 54,  166 => 53,  163 => 52,  158 => 50,  155 => 49,  150 => 47,  147 => 46,  145 => 45,  142 => 44,  137 => 42,  134 => 41,  129 => 39,  126 => 38,  124 => 37,  121 => 36,  116 => 34,  113 => 33,  108 => 31,  105 => 30,  103 => 29,  95 => 24,  90 => 22,  84 => 19,  76 => 13,  65 => 11,  61 => 10,  56 => 8,  50 => 7,  46 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "extension/module/report_setting.twig", "");
    }
}
