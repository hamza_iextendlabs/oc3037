<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/extension/module/seller_form.twig */
class __TwigTemplate_19cf80a14c93f5c6612b87d3f402cde0e31bf687a25aee3f17d0c2c56ef08826 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container\">
      <div>
      <h1>";
        // line 6
        echo ($context["text_seller_form"] ?? null);
        echo "</h1>
      <ul class=\"breadcrumb\">
        ";
        // line 8
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 9
            echo "        <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 9);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 9);
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 11
        echo "      </ul>
    </div>
  </div>
  <div class=\"container\">";
        // line 14
        if (($context["error_warning"] ?? null)) {
            // line 15
            echo "    <div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo ($context["error_warning"] ?? null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 19
        echo "    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\"><i class=\"fa fa-pencil\"></i> ";
        // line 21
        echo ($context["text_form"] ?? null);
        echo "</h3>
      </div>
      <div class=\"panel-body\">
        <form action=\"";
        // line 24
        echo ($context["action"] ?? null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal\">
          <div class=\"tab-content\">
            <div class=\"form-group required\">
              <label class=\"col-sm-2 control-label\" for=\"input-seller-name\">";
        // line 27
        echo ($context["entry_seller_name"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\">
                <input type=\"text\" name=\"seller_name\" value=\"";
        // line 29
        echo ($context["seller_name"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_seller_name"] ?? null);
        echo "\" id=\"input-seller-name\" class=\"form-control\" />
                ";
        // line 30
        if (($context["error_seller_name"] ?? null)) {
            // line 31
            echo "                <div class=\"text-danger\">";
            echo ($context["error_seller_name"] ?? null);
            echo "</div>
                ";
        }
        // line 32
        echo " </div>
            </div>
            <div class=\"form-group required\">
              <label class=\"col-sm-2 control-label\" for=\"input-seller-description\">";
        // line 35
        echo ($context["entry_seller_description"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\">
                <textarea name=\"seller_description\" placeholder=\"";
        // line 37
        echo ($context["entry_seller_description"] ?? null);
        echo "\" id=\"input-seller-description\" data-toggle=\"summernote\" data-lang=\"";
        echo ($context["summernote"] ?? null);
        echo "\" class=\"form-control\" style=\"height: 150px;\">";
        echo ($context["seller_description"] ?? null);
        echo "</textarea>
                ";
        // line 38
        if (($context["error_seller_description"] ?? null)) {
            // line 39
            echo "                <div class=\"text-danger\">";
            echo ($context["error_seller_description"] ?? null);
            echo "</div>
                ";
        }
        // line 40
        echo " </div>
            </div>
            <div class=\"form-group\">
              <label class=\"col-sm-2 control-label\" for=\"input-logo\">";
        // line 43
        echo ($context["entry_logo"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\"><a href=\"\" id=\"thumb-logo\" data-toggle=\"image\" class=\"img-thumbnail\"><img src=\"";
        // line 44
        echo ($context["thumb"] ?? null);
        echo "\" alt=\"\" title=\"\" data-placeholder=\"";
        echo ($context["placeholder"] ?? null);
        echo "\" /></a>
                <input type=\"hidden\" name=\"logo\" value=\"";
        // line 45
        echo ($context["logo"] ?? null);
        echo "\" id=\"input-logo\" />
              </div>
            </div>
            <div class=\"form-group\">
              <label class=\"col-sm-2 control-label\" for=\"input-benner-image\">";
        // line 49
        echo ($context["entry_banner_image"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\"><a href=\"\" id=\"thumb-benner-image\" data-toggle=\"image\" class=\"img-thumbnail\"><img src=\"";
        // line 50
        echo ($context["thumb_banner"] ?? null);
        echo "\" alt=\"\" title=\"\" data-placeholder=\"";
        echo ($context["placeholder"] ?? null);
        echo "\" /></a>
                <input type=\"hidden\" name=\"banner_image\" value=\"";
        // line 51
        echo ($context["banner_image"] ?? null);
        echo "\" id=\"input-banner-image\" />
                </div>
            </div>
            <div class=\"form-group required\">
              <label class=\"col-sm-2 control-label\" for=\"input-email\">";
        // line 55
        echo ($context["entry_email"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\">
                <input type=\"text\" name=\"email\" value=\"";
        // line 57
        echo ($context["email"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_email"] ?? null);
        echo "\" id=\"input-email\" class=\"form-control\" />
                ";
        // line 58
        if (($context["error_email"] ?? null)) {
            // line 59
            echo "                <div class=\"text-danger\">";
            echo ($context["error_email"] ?? null);
            echo "</div>
                ";
        }
        // line 60
        echo "</div>
            </div>
          </div>
        </form>
      </div>
    </div>
    <div class=\"pull-right\">
      <button type=\"submit\" form=\"form-seller\" data-toggle=\"tooltip\"  class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></button>
      <a href=\"";
        // line 68
        echo ($context["cancel"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_cancel"] ?? null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
    </div>
  </div>
  <link href=\"view/javascript/codemirror/lib/codemirror.css\" rel=\"stylesheet\" />
  <link href=\"view/javascript/codemirror/theme/monokai.css\" rel=\"stylesheet\" />
  <script type=\"text/javascript\" src=\"view/javascript/codemirror/lib/codemirror.js\"></script> 
  <script type=\"text/javascript\" src=\"view/javascript/codemirror/lib/xml.js\"></script> 
  <script type=\"text/javascript\" src=\"view/javascript/codemirror/lib/formatting.js\"></script> 
  <script type=\"text/javascript\" src=\"view/javascript/summernote/summernote.js\"></script>
  <link href=\"view/javascript/summernote/summernote.css\" rel=\"stylesheet\" />
  <script type=\"text/javascript\" src=\"view/javascript/summernote/summernote-image-attributes.js\"></script> 
  <script type=\"text/javascript\" src=\"view/javascript/summernote/opencart.js\"></script> 
  <script type=\"text/javascript\"><!--
\$('#language a:first').tab('show');
//--></script></div>
";
        // line 83
        echo ($context["footer"] ?? null);
        echo " 
";
    }

    public function getTemplateName()
    {
        return "default/template/extension/module/seller_form.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  230 => 83,  210 => 68,  200 => 60,  194 => 59,  192 => 58,  186 => 57,  181 => 55,  174 => 51,  168 => 50,  164 => 49,  157 => 45,  151 => 44,  147 => 43,  142 => 40,  136 => 39,  134 => 38,  126 => 37,  121 => 35,  116 => 32,  110 => 31,  108 => 30,  102 => 29,  97 => 27,  91 => 24,  85 => 21,  81 => 19,  73 => 15,  71 => 14,  66 => 11,  55 => 9,  51 => 8,  46 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/extension/module/seller_form.twig", "");
    }
}
